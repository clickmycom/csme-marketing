<link rel="stylesheet" type="text/css" media="all" href="http://3dp.contentsme.net/pub/static/frontend/Smartwave/porto/en_AU/owl.carousel/assets/owl.carousel.css">
<script type="text/javascript" src="http://3dp.contentsme.net/pub/static/frontend/Smartwave/porto/en_AU/owl.carousel/owl.carousel.min.js"></script>
<?php
/**
 * @version $Id: slideshow.php 58 2015-06-10 12:15:24Z szymon $
 * @package DJ-MediaTools
 * @copyright Copyright (C) 2012 DJ-Extensions.com LTD, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Szymon Woronowski - szymon.woronowski@design-joomla.eu
 *
 * DJ-MediaTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-MediaTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-MediaTools. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die ('Restricted access'); ?>

<div style="border: 0px !important;">
<div id="dj-slideshow<?php echo $mid; ?>" class="dj-slideshow">

	<?php if($params->get('show_custom_nav') && $params->get('custom_nav_pos')=='above') { ?>
		<div class="dj-indicators <?php echo ($params->get('show_custom_nav')==2 ? 'showOnMouseOver' : ''); ?>">
			<div class="dj-indicators-in">
				<?php for($i = 1; $i <= count($slides); $i++) { ?>
					<span class="dj-load-button<?php if ($i == 1) echo ' dj-load-button-active'; ?>"><span class="dj-key"><?php echo $i; ?></span></span>
				<?php } ?>
			</div>
        </div>
	<?php } ?>


		<!-- <h2 class="filterproduct-title"><span class="content"><strong>New Products</strong></span></h2> -->
		<div class="col-md-12">
			<div id="owl-example" class="owl-carousel">

				<?php foreach ($slides as $slide) {?>
									<div class="product-thumb23 ">
										<a href="<?=$slide->link?>">
											<header class="product-header">

													<?php

													$output = preg_replace('/(<[^>]+) style=".*?"/i','$1',$slide->description);

													$part2= explode("</p>",$output);
													$part= explode(" ",$part2[0]);
													?>


													<?php


													$x = 	explode("media/djcatalog2/images/",$slide->image);
													$part_img = DJCatalog2ImageHelper::getImageUrl($x[1]);
													if((isset($part_img))){
														list($width, $height) = getimagesize($part_img);
														$sum_height= $height/2;

														switch ($width>=$sum_height) {
															case TRUE:
																$wat= "img_contractors";
																$style = "width: 100%;";
																break;

															case FALSE:
																	$wat = "img_min_fix_width";
																	$style = "width: 40%;";
																break;

															default:
																$wat= "img_contractors";
																$style = "";
																break;
														}

													}


											?>

													<img class="<?=$wat?> item product-item-photo lazyOwl" style="<?=$style?>" src="<?=$slide->image?>" alt="<?=$slide->title?>" title="<?=$slide->title?>">

											</header>
										</a>
											<div class="product-inner product-reviews-summary tk ">
													<h5 class="product-title"><?=$slide->title?></h5>
													<?php
															$output = preg_replace('/(<[^>]+) style=".*?"/i','$1', $slide->description);
															$str = strip_tags($output,'<p></p>');
													 ?>

														 <div class="tk">
															 			 <p ><?=$str?></p>
														 </div>
											</div>
										</div>
		<?php } ?>

			</div>
			<div style="display:none;" class="dj-loader"></div>
		</div>
</div>
<div style="clear: both"></div>
<?php
	//print_r($slides);
	// exit;
 ?>

<script type="text/javascript">
jQuery(document).ready(function() {

                        jQuery("#owl-example").owlCarousel({
                            autoplay: true,
                            autoplayTimeout: 1500,
                            autoplayHoverPause: true,
                            loop: true,
                            navRewind: true,
                            margin: 10,
                            nav: true,
                            navText: ["<em class='owl-prev-left'></em>","<em class=''></em>"],
                            dots: false,
                            responsive: {
                                0: {
                                    items:2
                                },
                                640: {
                                    items:3
                                },
                                768: {
                                    items:4
                                },
                                992: {
                                    items:5
                                },
                                1200: {
                                    items:6
                                }
                            }
                        });
						});
</script>


<style>
.product-item-photo {
    display: block;
    position: relative;
    overflow: hidden;
    padding: 4px;
		width: 34%;
    border-radius: 7px;
}
 .product-inner {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    background: #fff;
    padding: 2px 22px;
    border-top: none;
    position: relative;
    -webkit-border-radius: 0 0 5px 5px;
}
.product-thumb23{
	border-radius: 0 0 5px 5px;
	border: 1px solid #ddd;
	border-radius: 4px;
	height: 400px;

}
.product-thumb23:hover {
   border-color: #0088CC;
}


.view_readmore{
border-top: 1px dashed #e6e6e6;
position: fixed;
color: #06c;
bottom: 10px;
padding: 0 0px;
}

.tk{
	font-size:11.5px;
	display:block;
	height:110px;
	word-wrap:break-word;

}

.filterproduct-title {
/*background: url("/templates/jm-joomads-ef4/images/prev.png") right no-repeat;*/
    font-size: 16px;
    font-weight: 300;
    line-height: 42px;
    margin: 0;
    color: #313131;
    text-transform: uppercase;
    text-align: left;
	}

.owl-prev ,.owl-next{
	/*background: url("http://3dp.contentsme.net/pub/static/frontend/Smartwave/porto/en_AU/images/slider-bar.png") right no-repeat;*/
    width: 22px;
    height: 40px;
    margin-top: -20px;
    position: absolute;
    top: 50%;
}
.owl-next{
	background: url("/templates/jm-joomads-ef4/images/next.png") right no-repeat;
  right: -43px;
	width: 30px;
	height: 100%;
	top:10px;
}
.owl-prev{
	background: url("/templates/jm-joomads-ef4/images/prev.png") right no-repeat;
	left: -45px;
	width: 30px;
	height: 100%;
	top:10px;
}
@media only screen and (max-width: 400px){
	.owl-next{
		right: -38px;
	}
	.owl-prev{
		left: -36px;
	}
}
.owl-stage-outer{

}


</style>
