html,
body {
  height: 100%;
}
html.no-scroll {
  width: 100%;
  overflow-y: scroll;
  position: fixed;
}
#jm-allpage {
  -webkit-transition: 300ms ease all;
  -moz-transition: 300ms ease all;
  -o-transition: 300ms ease all;
  transition: 300ms ease all;
}
.off-canvas-right.off-canvas #jm-allpage {
  margin-left: -300px;
  margin-right: 300px;
}
.off-canvas-left.off-canvas #jm-allpage {
  margin-right: -300px;
  margin-left: 300px
;}
#jm-offcanvas {
  width: 300px;
  height: 100%;
  position: fixed;
  z-index: 9999;
  background: #333333;
  top: 0;
  -webkit-transition: 300ms ease all;
  -moz-transition: 300ms ease all;
  -o-transition: 300ms ease all;
  transition: 300ms ease all;
}
.off-canvas-right #jm-offcanvas {
  right: -300px;
}
.off-canvas-right.off-canvas #jm-offcanvas {
  right: 0;
  overflow-y: auto;
}
.off-canvas-left #jm-offcanvas {
  left: -300px;
}
.off-canvas-left.off-canvas #jm-offcanvas {
  left: 0;
  overflow-y: auto;
}
#jm-offcanvas-toolbar {
  background: #292929;
  height: 34px;
  line-height: 34px;
  padding: 0 15px;
}
#jm-offcanvas-content {
  padding: 15px;
  color: #ffffff;
}
#jm-offcanvas-content .jm-title {
  color: #ffffff;
}
.toggle-nav {
  cursor: pointer;
}
.toggle-nav [class^="icon-"],
.toggle-nav [class*=" icon-"] {
  font-size: 20px;
}
.toggle-nav.menu {
  display: inline-block;
  color: #808080;
  text-align: center;
  vertical-align: top;
}
.toggle-nav.close-menu {
  color: #ffffff;
}
.off-canvas-left #jm-offcanvas-toolbar {
  text-align: right;
}
html[dir='rtl'] .off-canvas-right #jm-offcanvas-toolbar {
  text-align: left;
}
#jm-top-bar .toggle-nav.menu {
  height: 34px;
  color: #808080;
}
#jm-top-bar .toggle-nav.menu [class^="icon-"],
#jm-top-bar .toggle-nav.menu [class*=" icon-"] {
  top: 0;
  height: 34px;
  line-height: 34px;
}


#left_column .left-module{
	margin-bottom: 30px;
	overflow: hidden;
}
#left_column .left-module .owl-dots{
	bottom: 5px;
}
#left_column .left-module:last-child{
	margin-bottom: 0;
}
#left_column .left-module img{
	margin: 0 auto;
}
#left_column .block{
	border: 1px solid #eaeaea;
}
#left_column .block .title_block{
	font-size: 14px;
	font-weight: normal;
	border-bottom: 1px solid #eaeaea;
	padding-left: 19px;
	padding-top: 16.5px;
	padding-bottom: 16.5px;
}
#left_column .block .block_content{
	padding:  15px 20px;
}
.layered .layered_subtitle{
	color: #666;
	font-size: 16px;
	padding-bottom: 4px;
	text-transform: uppercase;
}


/*Span icon Star*/
.star_icon{
    color: #f9d408;
    font-size: inherit;
    position: relative;
    font-weight: normal;
    top: 3px;
    text-align: center;
    left: 10px;
}
/*Span For Sale*/
.for_sale{
  font-size: 55% !important;
  position: relative;
  top: -2px;
}
.promo{
  font-size: 55% !important;
  position:relative;
  left:15px;
  top: -2px;
}
.product_border_img{
  border: 3px solid #fff;
  overflow: hidden;
}
.product_border_right{
  border-right: 2px solid #fff;
  overflow: hidden;
}

/*//////////////////*/
/*Test Test Test */
/*/////////////////*/
.owl-carousel .product-thumb {
  margin: 5px 0;
}
.product-buy-counter {
  position: absolute;
  bottom: 10px;
  width: 100%;
  left: 0;
  z-index: 7;
}
.product-buy-counter > span {
  display: inline-block;
  padding: 5px 7px;
  background: rgba(0,0,0,0.7);
  color: #fff;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  font-size: 12px;
}
.product-buy-counter > span .fa {
  margin-right: 5px;
}
.product-logo {
  display: inline-block;
  max-width: 50px;
  margin-bottom: 7px;
  opacity: 0.7;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)";
  filter: alpha(opacity=70);
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
}
.product-secondary-image {
  z-index: 3;
}
.product-secondary-image > img {
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
}
.product-title + .product-meta {
  margin-top: 7px !important;
}
.product-header {
  position: relative;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
  /*border: 1px solid #eaeaea;padding: 10px*/
}
.product-quick-view,
.product-secondary-image {
  opacity: 0;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: alpha(opacity=0);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0,0,0,0.3);
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
}
.product-quick-view {
  z-index: 4;
}
.product-quick-view .fa {
  height: 35px;
  line-height: 35px;
  width: 35px;
  display: block;
  text-align: center;
  background: rgba(255,255,255,0.8);
  color: #525252;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  margin: 0 auto;
  top: 50%;
  margin-top: -17px;
  position: relative;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
}
.product-quick-view .fa:hover {
  background: #2a8fbd;
  color: #fff;
}
.product-thumb:hover .product-quick-view,
.product-thumb:hover .product-secondary-image,
.product-thumb:hover .product-logo {
  opacity: 1;
  -ms-filter: none;
  filter: none;
}
.product-category-icon {
  position: absolute;
  right: 15px;
  top: 15px;
  height: 50px;
  width: 50px;
  line-height: 50px;
  display: block;
  background: #2a8fbd;
  color: #fff;
  text-align: center;
  font-size: 25px;
  z-index: 5;
  opacity: 0.5;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
  filter: alpha(opacity=50);
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
}
.product-label {
  position: absolute;
  left: 15px;
  top: 15px;
  padding: 4px 8px;
}
.product-search-title {
  font-size: 24px;
  margin-bottom: 40px;
}
.product-desciption {
  color: #292f33;
  font-size: 12px;
  margin-bottom: 0;
  line-height: 1.4em;
}
.product-time {
  color: #666;
  font-weight: 600;
  font-size: 13px;
  display: block;
  margin-bottom: 10px;
}
.product-location {
  border-top: 1px dashed #e6e6e6;
  margin-top: 13px;
  padding-top: 8px;
  line-height: 1em;
  margin-bottom: 0;
  color: #a3a3a3;
  font-size: 12px;
}
.product-thumb {
  z-index: 1;
  position: relative;
  text-decoration: none !important;
  display: block;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
  text-align: center;
  /*-webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
  box-shadow: 0 1px 1px rgba(0,0,0,0.2);*/
  -webkit-border-radius: 5px;
  border-radius: 5px;
}
.product-thumb .product-header >img {
  display: block;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
}
.product-thumb .product-inner {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  /*padding: 20px 22px;*/
  /*padding: 12px;*/
  border-top: none;
  position: relative;
  -webkit-border-radius: 0 0 5px 5px;
  border-radius: 0 0 5px 5px;
}
.product-thumb .product-title {
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -o-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
  z-index: 2;
  margin-bottom: 5px;
  font-size: 16px;
}
.product-thumb .icon-list-rating {
  font-size: 11px;
  color: #48aad6;
  margin-bottom: 4px;
}
.product-thumb .icon-list-non-rated {
  color: #949494 !important;
}
.product-thumb .product-non-rated {
  color: #ccc;
}
.product-thumb .product-meta {
  margin-top: 15px;
}
.product-thumb .product-price-list,
.product-thumb .product-actions-list {
  list-style: none;
  margin: 0;
  padding: 0;
}
.product-thumb .product-price-list > li,
.product-thumb .product-actions-list > li {
  margin-right: 15px;
  display: inline-block;
}
.product-thumb .product-price-list > li:last-child,
.product-thumb .product-actions-list > li:last-child {
  margin-right: 0;
}
.product-thumb .product-actions-list {
  padding-top: 15px;
  margin-top: 15px;
  /*border-top: 1px dashed #CFCFCF;*/
}
.product-thumb .product-price-list {
  font-size: 13px;
  margin-bottom: 0;
}
.product-thumb .product-price {
  font-weight: bold;
  font-size: 16px;
  color: #333;
}
.product-inner .product-location-link {
  text-decoration: none;
  color: #333;
}
.product-thumb .price-negotiable {
  font-weight: normal;
  color: #333;
}
.product-thumb .product-old-price {
  color: #858585;
  text-decoration: line-through;
}
.product-thumb .product-save {
  font-weight: 600;
}
.product-thumb .product-category {
  font-size: 12px;
  color: #a3a3a3;
  margin-bottom: 0;
  margin-top: 15px;
  line-height: 1em;
  text-transform: lowercase;
  font-style: italic;
}
.product-thumb .product-category > i {
  color: #949494;
  display: block;
  margin-bottom: 3px;
  font-size: 13px;
}
/*.product-thumb:hover {
  -webkit-transform: translate3d(0, -5px, 0);
  -moz-transform: translate3d(0, -5px, 0);
  -o-transform: translate3d(0, -5px, 0);
  -ms-transform: translate3d(0, -5px, 0);
  transform: translate3d(0, -5px, 0);
  -webkit-box-shadow: 0 4px 2px rgba(0,0,0,0.25);
  box-shadow: 0 4px 2px rgba(0,0,0,0.25);
}*/
.product-thumb:hover .product-category-icon {
  opacity: 1;
  -ms-filter: none;
  filter: none;
}
.product-thumb-horizontal {
  margin-bottom: 30px;
}
@media (min-width:992px) {
.product-thumb-horizontal {
  background: #fff;
  overflow: hidden;
  text-align: left;
}
.product-thumb-horizontal .product-header {
  float: left;
  /*width: 30%;*/
  width: auto;
  -webkit-border-radius: 0 5px 0 5px;
  border-radius: 0 5px 0 5px;
}
.product-thumb-horizontal .product-header > img {
  -webkit-border-radius: 0;
  border-radius: 0;
}
.product-thumb-horizontal .product-inner {
  float: left;
  padding-left: 15px;
  display: block;
  width: 70%;
  padding-right: 30%;
}
.product-thumb-horizontal .product-meta {
  position: absolute;
  width: 40%;
  margin-top: 0;
  top: 0;
  right: 0;
  padding: 20px 22px;
  text-align: right;
}
}
.product-thumb-hold {
  -webkit-box-shadow: 0 2px 1px rgba(0,0,0,0.15);
  box-shadow: 0 2px 1px rgba(0,0,0,0.15);
}
.product-thumb-hold >img {
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
}


.product-page-meta .product-page-meta-info {
  margin-top: 30px;
}
.product-page-meta .product-page-meta-info .product-page-meta-price {
  font-size: 17px;
  display: block;
  text-align: center;
  font-weight: 600;
}
.product-page-meta .product-page-meta-info .product-page-meta-title {
  font-size: 13px;
  display: block;
}
.product-page-meta .product-page-meta-info > li {
  margin-top: 10px;
  padding-top: 10px;
  border-top: 1px solid #e6e6e6;
}
.product-page-meta .product-page-meta-info .product-page-meta-price-list {
  overflow: hidden;
}
.product-page-meta .product-page-meta-info .product-page-meta-price-list > li {
  float: left;
  margin-right: 20px;
}
.product-page-meta .product-page-meta-info .product-page-meta-price-list > li:last-child {
  margin-right: 0;
}
.product-search-results {
  overflow: hidden;
}
.product-search-results >li {
  margin-bottom: 20px;
  padding-bottom: 20px;
  border-bottom: 1px solid #ededed;
}
.product-search-results .product-inner {
  display: table;
}
.product-search-results .product-search-thumb {
  float: left;
  margin-right: 15px;
}
.product-search-results .product-price {
  background: #2a8fbd;
  color: #fff;
  padding: 0 10px;
  height: 26px;
  line-height: 26px;
  display: inline-block;
  font-size: 13px;
  font-weight: bold;
  margin-bottom: 5px;
}
.product-search-results .product-title {
  margin: 3px 0 10px 0;
  font-size: 17px;
  line-height: 17px;
}
.product-search-results .product-time {
  font-size: 14px;
}
.product-search-results .product-meta {
  font-style: italic;
  overflow: hidden;
  margin-bottom: 5px;
}
.product-search-results .product-meta >li {
  float: left;
  margin-right: 5px;
  padding-right: 5px;
  border-right: 1px solid #ccc;
}
.product-search-results .product-meta >li:last-child {
  margin-right: 0;
  padding-right: 0;
  border-right: none;
}
.product-wishlist-remove {
  margin-top: 10px;
  text-align: center;
}
.flexnav-coupon li > a [class^="fa fa-"] {
  font-size: 17px;
  margin-right: 3px;
}
@media (min-width: 800px) {
  .flexnav-coupon > li {
    margin-bottom: 3px;
  }
}
.product-sort {
  position: relative;
  margin-bottom: 15px;
  font-size: 13px;
  display: table;
}
.product-sort b {
  font-weight: 600;
}
.product-sort .product-sort-title {
  margin-right: 5px;
}
.product-sort .product-sort-selected {
  display: inline-block;
  padding: 5px 12px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  background: #fff;
  border: 1px solid #e6e6e6;
}
.product-sort .product-sort-order {
  margin-left: 4px;
  font-size: 15px;
  display: inline-block;
  width: 30px;
  height: 30px;
  line-height: 28px;
  text-align: center;
  color: #757575;
  background: #fff;
  border: 1px solid #e6e6e6;
  -webkit-border-radius: 3px;
  border-radius: 3px;
}
.product-sort > ul {
  list-style: none;
  margin: 0;
  padding: 5px 0 0 0;
  position: absolute;
  z-index: 5;
  display: none;
}
.product-sort > ul > li > a {
  padding: 5px 12px;
  display: block;
  color: #666;
  background: #fff;
  border: 1px solid #e6e6e6;
  border-bottom: none;
  font-size: 12px;
}
.product-sort > ul > li > a:hover {
  background: #f7f7f7;
}
.product-sort > ul > li:first-child > a {
  -webkit-border-radius: 3px 3px 0 0;
  border-radius: 3px 3px 0 0;
}
.product-sort > ul > li:last-child > a {
  -webkit-border-radius: 0 0 3px 3px;
  border-radius: 0 0 3px 3px;
  border-bottom: 1px solid #e6e6e6;
}
.product-sort:hover > ul {
  display: block;
}
.product-view > .fa {
  display: inline-block;
  width: 30px;
  height: 30px;
  background: #fff;
  line-height: 28px;
  border: 1px solid #e6e6e6;
  text-align: center;
  color: #666;
  -webkit-border-radius: 3px;
  border-radius: 3px;
}
.product-view > .fa:first-child {
  margin-right: 5px;
}
.product-view > .fa.active {
  background: #666;
  border-color: #4d4d4d;
  color: #fff;
  cursor: default;
}
.product-info-price {
  font-size: 50px;
  color: #2a8fbd;
  margin-bottom: 10px;
  padding-bottom: 10px;
  border-bottom: 1px solid #d9d9d9;
  line-height: 1em;
  letter-spacing: -3px;
}
.product-info-list {
  margin-bottom: 20px;
}
.product-banner {
  display: block;
  position: relative;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
  background: #11394c;
}
.product-banner > img {
  -webkit-border-radius: 5px;
  border-radius: 5px;
  -webkit-transition: 0.3s;
  -moz-transition: 0.3s;
  -o-transition: 0.3s;
  -ms-transition: 0.3s;
  transition: 0.3s;
  display: block;
}
.product-banner .product-banner-inner {
  position: absolute;
  text-align: center;
  width: 100%;
  bottom: 20px;
}


.sale-point {
  text-align: center;
}
.sale-point .sale-point-icon {
  display: inline-block;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 25px;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  color: #fff;
  background: #333;
  margin-bottom: 12px;
}
.sale-point .sale-point-title {
  font-size: 19px;
  margin-bottom: 5px;
}
.sale-point .sale-point-description {
  font-size: 13px;
  color: #7d7d7d;
  margin: 0 auto;
  width: 70%;
}
.edia-heading_t{
  margin-top: 8px;
  margin-bottom: 15px;
  font-size: 13.5px;
      color: #292f33;
}
.view_detail{
  float: left;
  position: fixed;
  color: #06c;
  bottom: 10px;
  padding: 0 10px;
  right: 0px;
}
.img_contractors{

/*width: 252.25px;*/
height: 196px;

padding: 10px;
border-radius: 4px;

}
.img_min{
    height:196px;
    /*border: 1px solid #eaeaea;*/
    padding: 5px;
    margin: 0 auto;
    /*border: 1px solid #eaeaea;padding: 10px;*/
}
.img_min_fix_width{
  position: relative;
  height:196px;
  display: block;
  margin: 0 auto;
  /*border: 1px solid #eaeaea;padding: 10px;*/
}
.box_i{
  display: block;margin: 0 auto;width:210px;
}

.color_5d9919{
color:#006937;
}










/*--------------*/
/*--Search Box*--/
/*--------------*/



/** Clear **/

.c-clear {
    clear: both;
}


/****************************
 Components
****************************/

.header {
    position: relative;
    overflow-x: visible;
}

#homenew .header {
    margin-bottom: 20px;
}

body#srchrslt .header__banner-container,
body#vip .header__banner-container {
    background: #92c100;
    min-height: 80px;
}

.header__logo-container {
    position: relative;
}

.header__logo-link {
    width: 100px;
    height: 60px;
    display: block;
    position: absolute;
    top: 0px;
    left: 0px;
    overflow: hidden;
    z-index: 30;
}

.header--mobile-menu-open .header__logo-link,
.header--search-open .header__logo-link,
.header--all-categories-open .header__logo-link {
    display: none;
}

.header__logo-title {
    display: none;
}

.header__logo-image {
    width: 80px;
    height: auto;
    position: absolute;
    top: -8px;
    left: 3px;
}

.header--mobile-menu-open .header__logo-image,
.header--search-open .header__logo-image {
    display: none;
}

.header__upper-deck {
    background: #92c100;
    height: 60px;
}

.header__upper-deck-wrapper {
    max-width: 1280px;
    margin: 0 auto;
}

.header--mobile-menu-open .header__upper-deck-wrapper {
    position: fixed;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 3;
    padding: 60px 0 0 0;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
    -webkit-transform: translate3d(0, 0, 0);
    box-sizing: border-box;
    background: #ecebe8;
}

.header--mobile-menu-open .header__upper-deck-wrapper:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 60px;
    background-color: #92c100;
    z-index: 2;
}

.header--mobile-menu-open .header__upper-deck-wrapper:after {
    position: fixed;
    background: #ecebe8;
    top: 0;
    left: 0;
    right: 0;
    bottom: -1000px;
    content: '';
    z-index: 1;
    -webkit-transform: translate3d(0, 0, 0);
}

.header__upper-deck-list {
    margin: 0;
    padding: 0;
    text-align: right;
    height: 100%;
    position: relative;
    z-index: 4;
}

.header__upper-deck-item {
    list-style: none;
    display: inline-block;
    position: relative;
    height: 60px;
    line-height: 60px;
    vertical-align: top;
}

.header__upper-deck-item:after {
    content: '';
    background: #c9e080;
    width: 1px;
    height: 12px;
    position: absolute;
    top: 32px;
    right: 0;
}

.header__upper-deck-item:last-child:after {
    display: none;
}

.header__upper-deck-item--logo {
    position: absolute;
    top: 0;
    left: 0;
}

.header__upper-deck-item--logo:after,
.header__upper-deck-item--post-ad:after {
    display: none;
}

.header__upper-deck-item--message-center {
    position: absolute;
    top: -7px;
    right: 50px;
    overflow: hidden;
}

.header__upper-deck-item--message-center:after {
    display: none;
}

.header__upper-deck-item--message-center,
.header__upper-deck-item--my-gumtree,
.header__upper-deck-item--register,
.header__upper-deck-item--signin {
    padding: 0;
}

.header__upper-deck-item--help,
.header__upper-deck-item--my-gumtree,
.header__upper-deck-item--sign-in-welcome,
.header__upper-deck-item--post-ad,
.header__upper-deck-item--register,
.header__upper-deck-item--signin,
.header__upper-deck-item--signout {
    display: none;
}

.header__upper-deck-item--post-ad {
    padding: 10px;
    box-sizing: border-box;
}

.header__upper-deck-item--my-gumtree {
    width: 195px;
}

.header__upper-deck-item--my-gumtree:after {
    display: none;
}

.header__upper-deck-item--help:hover:before,
.header__upper-deck-item--my-gumtree:hover:before {
    border-top-color: #c9e080;
}

.header__upper-deck-item--mobile-menu-trigger {
    display: block;
}

.header__upper-deck-item--show-desktop {
    display: none;
}

.header--search-open .header__upper-deck-item--mobile-menu-trigger {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--my-gumtree,
.header--mobile-menu-open .header__upper-deck-item--post-ad {
    display: block;
    width: 100%;
    height: auto;
}

.header--mobile-menu-open .header__upper-deck-item--my-gumtree.header__upper-deck-item--show-desktop,
.header--mobile-menu-open .header__upper-deck-item--post-ad.header__upper-deck-item--show-desktop {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--register,
.header--mobile-menu-open .header__upper-deck-item--signin {
    display: block;
    width: 50%;
    float: right;
    padding: 0 10px 0 5px;
    box-sizing: border-box;
    text-align: center;
}

.header--mobile-menu-open .header__upper-deck-item--register:after,
.header--mobile-menu-open .header__upper-deck-item--signin:after {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--message-center {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--signout {
    display: block;
    width: 100%;
    padding: 0 10px 0 10px;
    box-sizing: border-box;
    text-align: center;
    color: #fff;
}

.header--mobile-menu-open .header__upper-deck-item--signout:after {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--signin {
    padding: 0 5px 0 10px;
    float: left;
}

.header--mobile-menu-open .header__upper-deck-item--mobile-menu-trigger {
    position: absolute;
    top: 0;
    right: 0;
}

.header__upper-deck-item-link {
    text-decoration: none;
    color: #222;
    border-radius: 3px;
    border: 2px solid #92c100;
    display: block;
    height: 44px;
    line-height: 44px;
    font-weight: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    padding: 0 26px;
    box-sizing: border-box;
}

.header__upper-deck-item-link:hover,
.header__upper-deck-item-link:active,
.header__upper-deck-item-link:visited {
    color: #222;
}

.header__upper-deck-item-link--sign-out {
    display: block;
}

.header__mobile-post-ad-button {
    margin-top: 50px;
}

.header__post-ad-button {
    background-color: #ff6002;
    color: #fff;
    width: 100%;
    height: 44px;
    line-height: 44px;
    font-size: 14px;
    border-radius: 2px;
    text-decoration: none;
    text-align: center;
    display: block;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    letter-spacing: 0.5px;
}

.header__post-ad-button:hover,
.header__post-ad-button:active,
.header__post-ad-button:visited {
    color: #fff;
}

.header__post-ad-button--loading {
    background-color: #823000 !important;
}

.header__help-link {
    text-decoration: none;
}

.header__help-text {
    display: none;
}

.header__help-icon {
    border-radius: 100%;
    width: 25px;
    height: 25px;
    background: #fff;
    color: #92c100;
    font-size: 25px;
    line-height: 25px;
    text-align: center;
}

.header__message-center-wrapper {
    position: relative;
    display: block;
    z-index: 1;
    padding: 0 20px;
}

.header__message-center-image {
    width: 33px;
    height: 33px;
    fill: #fff;
    margin: 20px 6px 0 0;
}

.header__message-center-count {
    position: absolute;
    top: 18px;
    right: 28px;
    width: 18px;
    height: 18px;
    border-radius: 100%;
    background-color: #ff6002;
    color: #fff;
    line-height: 18px;
    text-align: center;
    font-size: 10px;
    opacity: 0;
    -webkit-transition: opacity 0.2s ease-in;
    transition: opacity 0.2s ease-in;
}

.header__message-center-count--has-loaded {
    opacity: 1;
}

.header__my-gumtree-trigger {
    display: none;
}

.header__my-gumtree-trigger-text {
    color: #fff;
    position: absolute;
    top: 1px;
    left: 80px;
    font-size: 13px;
}

.header__my-gumtree-portrait {
    width: 40px;
    height: 40px;
    display: block;
    margin-top: 17px;
    margin-left: 5px;
    -webkit-transition: opacity 0.2s ease-in;
    transition: opacity 0.2s ease-in;
    fill: #fff;
    position: absolute;
    z-index: 1;
    border-radius: 50%;
}

.header__my-gumtree-text {
    display: none;
}

.header__my-gumtree-user-text {
    font-size: 13px;
    color: #fff;
    margin-right: 9px;
    max-width: 200px;
    text-overflow: ellipsis;
    overflow: hidden;
    display: block;
    white-space: nowrap;
    display: none;
}

.header__my-gumtree-user-name {
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
}

.header--mobile-menu-open .header__my-gumtree-text {
    display: block;
    position: absolute;
    top: -60px;
    left: 25%;
    width: 50%;
    font-size: 18px;
    text-align: center;
    z-index: 30;
    line-height: 60px;
    color: #fff;
}

.header__my-gumtree-list {
    display: none;
}

.header--mobile-menu-open .header__my-gumtree-list {
    display: block;
    background: #fff;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
}

.header__my-gumtree-item {
    list-style: none;
}

.header__my-gumtree-item:first-child .header__my-gumtree-link {
    border-top: 0;
}

.header__my-gumtree-item:last-child .header__my-gumtree-link {
    border-bottom: 1px solid #d4d4d4;
}

.header__my-gumtree-link {
    text-decoration: none;
    display: block;
    width: 100%;
    text-align: left;
    line-height: 50px;
    border-top: 1px solid #d4d4d4;
    padding: 0 20px;
    box-sizing: border-box;
    color: #222;
}

.header__my-gumtree-link:hover {
    color: #222;
}

.header__my-gumtree-item-icon {
    width: 19px;
    height: 19px;
    fill: #006937;
    float: left;
    margin: 13px 10px 0 0;
}

.header__my-gumtree-item-icon--messages {
    width: 26px;
    height: 26px;
    margin-left: -5px;
    margin-right: 8px;
    margin-top: 11px;
}

.header--mobile-menu-open .header__lower-deck {
    display: none;
}

.header__mobile-menu-trigger {
    position: absolute;
    top: 0;
    right: 0;
    width: 68px;
    height: 60px;
    text-indent: -9999px;
    z-index: 30;
}

.header__mobile-menu-trigger:before {
    content: '';
    position: absolute;
    right: 20px;
    top: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    box-shadow: 0 8px 0 0 #fff, 0 16px 0 0 #fff;
}

.header__mobile-menu-trigger:after {
    content: '';
    position: absolute;
    right: 20px;
    top: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    display: none;
}

.header--mobile-menu-open .header__mobile-menu-trigger,
.header--all-categories-open .header__mobile-menu-trigger {
    position: absolute;
    top: -60px;
}

.header--mobile-menu-open .header__mobile-menu-trigger:before,
.header--all-categories-open .header__mobile-menu-trigger:before {
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
    box-shadow: none;
    top: 27px;
}

.header--mobile-menu-open .header__mobile-menu-trigger:after,
.header--all-categories-open .header__mobile-menu-trigger:after {
    display: block;
    -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
    top: 27px;
}

.header__navigation-container {
    background: #ecebe8;
    border-bottom: 1px solid #d4d4d4;
}

.navless .header .header__navigation-container {
    border-bottom: 0;
}

.header__primary-navigation-outer-wrapper {
    max-width: 1280px;
    position: relative;
    margin: 0 auto;
}

.header__primary-navigation-wrapper {
    max-width: 1280px;
    margin: 0 auto;
    z-index: 1;
    position: relative;
    -ms-touch-action: none;
        touch-action: none;
    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;
    -webkit-text-size-adjust: none;
        -ms-text-size-adjust: none;
            text-size-adjust: none;
}

.header__primary-navigation-list {
    margin: 0;
    padding: 0 80px 0 0;
    white-space: nowrap;
    overflow: scroll;
    -webkit-overflow-scrolling: touch;
    box-sizing: border-box;
    -webkit-transform: translate3d(0, 0, 0);
    position: static;
    -webkit-tap-highlight-color: transparent;
    -webkit-transform: translateZ(0);
            transform: translateZ(0);
    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;
}

.header__primary-navigation-item {
    list-style: none;
    display: inline-block !important;
    padding: 0 10px;
    position: relative;
    text-align: left;
    margin-right: -5px;
    height: 44px;
    line-height: 44px;
    cursor: pointer;
}

.header__primary-navigation-item--more-categories {
    position: absolute;
    top: 0px;
    right: 5px;
    background: #e2e1df;
    width: 66px;
    border-left: 1px solid #d4d4d4;
    text-align: center;
    padding: 0;
    z-index: 1;
}

.header__primary-navigation-item--more-categories:after {
    display: none;
}

.header__primary-navigation-item--more-categories:before {
    content: '';
    width: 8px;
    height: 100%;
    position: absolute;
    top: 0;
    left: -8px;
    background: -webkit-linear-gradient(left, rgba(236, 235, 232, 0) 0%, #ecebe8 100%);
    background: linear-gradient(to right, rgba(236, 235, 232, 0) 0%, #ecebe8 100%);
}

.navless .header .header__primary-navigation-item {
    display: none !important;
}

.navless .header .header__primary-navigation-list {
    overflow: hidden;
}

.header__more-categories-small {
    display: block;
}

.header__more-categories-medium-up {
    display: none;
}

.header__primary-navigation-link {
    color: #006937;
    text-decoration: none;
    font-size: 14px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    cursor: pointer;
    display: block;
    -webkit-tap-highlight-color: transparent;
    outline-color: transparent;
    outline-style: none;
}

.header__primary-navigation-link:visited {
    color: #5d9919;
}

.header__primary-navigation-link:hover {
    color: #5d9919;
}

.header.header--all-categories-open .header__primary-navigation-link--more-categories {
    display: none;
}

.header.header--all-categories-open .header__primary-navigation-item--more-categories {
    z-index: 100;
}

.header__secondary-navigation-tablet-container {
    position: absolute;
    top: 0;
    left: 0;
}

.header__secondary-navigation-tablet-container .header__secondary-navigation-list {
    display: block;
    z-index: 10;
    visibility: visible;
    opacity: 1;
    top: 80px;
}

.header__secondary-navigation-tablet-container .header__primary-navigation-link {
    display: none;
}

.header__secondary-navigation-list {
    margin: 0;
    padding: 0;
    display: none;
}

.header.header--all-categories-open .header__secondary-navigation-list-wrapper--all-categories {
    position: fixed;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 4;
    background: #fff;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
    padding-top: 60px;
    box-sizing: border-box;
    -webkit-transform: translate3d(0, 0, 0);
}

.header.header--all-categories-open .header__secondary-navigation-list-wrapper--all-categories:before {
    content: '';
    position: absolute;
    top: -60px;
    left: 0;
    width: 100%;
    height: 60px;
    background-color: #92c100;
    z-index: 2;
}

.header.header--all-categories-open .header__secondary-navigation-list-wrapper--all-categories:after {
    position: fixed;
    background: #fff;
    top: 0;
    left: 0;
    right: 0;
    bottom: -1000px;
    content: '';
    z-index: -1;
    -webkit-transform: translate3d(0, 0, 0);
}

.header.header--all-categories-open .header__secondary-navigation-list-wrapper--all-categories .header__secondary-navigation-list {
    display: block;
}

.header__all-categories-title {
    display: none;
}

.header--all-categories-open .header__all-categories-title {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    font-size: 18px;
    text-align: center;
    z-index: 30;
    line-height: 60px;
    color: #fff;
    height: 60px;
    background: #92c100;
}

.header--all-categories-open .header__mobile-close-all-categories {
    position: absolute;
    top: 0;
    right: 0;
    width: 60px;
    height: 60px;
}

.header--all-categories-open .header__mobile-close-all-categories:before {
    content: '';
    position: absolute;
    right: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
    top: 27px;
}

.header--all-categories-open .header__mobile-close-all-categories:after {
    content: '';
    position: absolute;
    right: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    display: block;
    -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
    top: 27px;
}

.header__primary-navigation-mobile-trigger {
    display: block;
}

.header__secondary-navigation-item {
    list-style: none;
    position: relative;
    z-index: 3;
    display: block !important;
}

.header__secondary-navigation-item--feature {
    cursor: default;
    padding: 20px 0 0 0;
    float: none;
    display: block;
    clear: both;
    width: 100%;
    position: absolute;
    top: 235px;
    left: 0;
    overflow: hidden;
}

.header__secondary-navigation-item--feature:before {
    content: '';
    left: -20px;
    right: -20px;
    height: 1px;
    position: absolute;
    top: 0;
    background-color: #d4d4d4;
    z-index: 2;
}

.header__secondary-navigation-item--feature:after {
    content: '';
    background-color: #fafafa;
    left: -20px;
    right: -20px;
    top: 0;
    bottom: 0;
    position: absolute;
    z-index: 0;
}

.header__secondary-navigation-item--is-dummy {
    display: none !important;
}

.header__secondary-navigation-item--is-dummy .header__secondary-navigation-link--is-dummy:hover {
    background-color: #fff;
}

.header__featured-image-wrapper {
    width: 33.33%;
    float: left;
    z-index: 2;
    position: relative;
    box-sizing: border-box;
    padding-left: 20px;
}

.header__featured-image {
    width: 100%;
    padding-right: 20px;
    box-sizing: border-box;
    box-sizing: border-box;
    padding-left: 20px;
}

.header__featured-text-wrapper {
    width: 66.66%;
    float: left;
    color: #222;
    z-index: 2;
    position: relative;
}

.header__featured-heading {
    font-size: 16px;
    line-height: 20px;
    margin: 15px 0;
}

.header__featured-paragraph {
    font-size: 14px;
    line-height: 20px;
    margin: 10px 0;
    padding-right: 35px;
}

.header__featured-link {
    font-size: 14px;
    display: inline-block;
    line-height: 20px;
}

.header__secondary-navigation-link {
    color: #222;
    text-decoration: none;
    font-size: 14px;
    display: block;
    position: relative;
    height: 50px;
    line-height: 50px;
    -webkit-tap-highlight-color: transparent;
    outline-color: transparent;
    outline-style: none;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header__secondary-navigation-link--is-dummy {
    cursor: default;
}

.header__secondary-navigation-icon {
    width: 19px;
    height: 19px;
    position: absolute;
    top: 15px;
    left: 17px;
    fill: #5d9919;
}

.header.header--all-categories-open .header__secondary-navigation-link {
    text-align: left;
    width: 100%;
    display: block;
    border-top: 1px solid #d4d4d4;
    color: #222;
    box-sizing: border-box;
    padding: 0 0 0 50px;
}

.header__secondary-navigation-down-icon {
    position: absolute;
    top: 1px;
    right: 0;
    width: 50px;
    height: 50px;
    display: block;
    z-index: 3;
}

.header__secondary-navigation-down-icon:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-top-color: #b7b7b7;
    top: 22px;
    right: 16px;
    display: block;
    z-index: 2;
}

.header__secondary-navigation-down-icon:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    width: 65px;
    height: 50px;
    background-color: #fff;
    z-index: 1;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header__secondary-navigation-down-icon--open:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-bottom-color: #b7b7b7;
    top: 14px;
    right: 16px;
}

.header__tertiary-navigation-list {
    margin: 0;
    padding: 0;
    display: none;
}

.header__tertiary-navigation-list--is-visible {
    display: block;
}

.header__tertiary-navigation-list .header__tertiary-navigation-list {
    padding-left: 20px;
}

.header__tertiary-navigation-item {
    list-style: none;
    position: relative;
}

.header__tertiary-navigation-item--feature {
    overflow: hidden;
    border-top: 1px solid #d4d4d4;
    padding-top: 10px;
    background-color: #fafafa;
    text-align: left;
}

.header__tertiary-navigation-item--feature .header__featured-image-wrapper {
    text-align: center;
}

.header__tertiary-navigation-item--feature .header__featured-image {
    max-width: 150px;
}

.header__tertiary-navigation-link {
    text-decoration: none;
    -webkit-tap-highlight-color: transparent;
    outline-color: transparent;
    outline-style: none;
    width: 100%;
    display: block;
    padding: 0 16px 0 60px;
    box-sizing: border-box;
    text-align: left;
    color: #222;
}

.header .j-selectbox__wrapper {
    -webkit-tap-highlight-color: transparent;
    overflow: hidden;
}

.header .j-selectbox__wrapper:after {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-top-color: #b7b7b7;
    top: 24px;
    right: 42px;
    z-index: 2;
}

.header .j-selectbox__wrapper:after {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-top-color: #b7b7b7;
    margin-top: -10px;

    top: 24px;
    right: 42px;
    z-index: 2;
}

.header .j-selectbox__wrapper--expanded {
    background-color: #5d9919 !important;
    overflow: visible;
}

.header .j-selectbox__ul {
    background: #fff;
    left: 0;
    text-align: left;
    min-width: 100%;
    z-index: 10;
    position: relative;
    border-bottom-right-radius: 3px;
    box-sizing: border-box;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    margin: -10px 0 10px 0;
    visibility: hidden;
    height: 0;
    opacity: 0;
    position: relative;
    top: 1px;
    display: none;
    -webkit-transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease, -webkit-transform 0.2s ease;
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
    will-change: transform, opacity;
}

.header .j-selectbox__wrapper--expanded .j-selectbox__ul {
    display: block;
    visibility: visible;
    height: auto;
    opacity: 1;
    -webkit-transform: translateY(0);
            transform: translateY(0);
}

.header .j-selectbox__child-ul {
    height: 0;
    -webkit-transition: height 0.2s ease-in;
    transition: height 0.2s ease-in;
}

.header .j-selectbox__child-ul--is-expanded {
    height: auto;
    padding-left: 0;
}

.header .j-selectbox__child-ul .j-selectbox__text {
    padding-left: 55px;
}

.header .j-selectbox__child-ul .j-selectbox__child-ul .j-selectbox__text {
    padding-left: 75px;
}

.header .j-selectbox__child-ul .j-selectbox__li:first-child:before {
    content: '';
    top: 0;
    left: 0;
    right: 0;
    height: 1px;
    background-color: #d4d4d4;
    position: absolute;
    z-index: 4;
}

.header .j-selectbox__child-ul .j-selectbox__li:after {
    left: 30px;
}

.header .j-selectbox__child-ul .j-selectbox__li .j-selectbox__li:first-child:before {
    left: 30px;
}

.header .j-selectbox__child-ul .j-selectbox__li .j-selectbox__li:after {
    left: 50px;
}

.header .j-selectbox__li {
    list-style: none;
    overflow: hidden;
    line-height: 50px;
    min-height: 50px;
    white-space: nowrap;
    position: relative;
}

.header .j-selectbox__li:after {
    content: '';
    bottom: 0;
    left: 0;
    right: 0;
    height: 1px;
    background-color: #d4d4d4;
    position: absolute;
    z-index: 4;
}

.header .j-selectbox__li:last-child {
    border-bottom: 0;
}

.header .j-selectbox__li-hover {
    background-color: #F5F5F3;
}

.header .j-selectbox__text {
    width: 100%;
    display: block;
    padding: 0 16px;
    box-sizing: border-box;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header .j-selectbox__svg-icon {
    width: 16px;
    height: 16px;
    fill: #5d9919;
    margin: 15px 14px 0 0;
    float: left;
}

.header .j-selectbox__dropdown-icon {
    width: 16px;
    height: 16px;
    fill: #5d9919;
    position: absolute;
    top: 19px;
    /*left: 16px;*/
    left: 32%;
    z-index: 7;
}

.header .j-selectbox__down-icon {
    position: absolute;
    top: 0;
    right: 0;
    width: 65px;
    height: 50px;
    display: block;
    z-index: 3;
}

.header .j-selectbox__down-icon:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-top-color: #b7b7b7;
    top: 22px;
    right: 13px;
    display: block;
    z-index: 2;
}

.header .j-selectbox__down-icon:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    width: 65px;
    height: 50px;
    background-color: #fff;
    z-index: 1;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header .j-selectbox__child-ul--is-expanded ~ .j-selectbox__down-icon:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-bottom-color: #b7b7b7;
    top: 14px;
    right: 13px;
}

.header .j-selectbox__dropdown-text {
    display: block;
    line-height: 55px;
    text-align: left;
    padding-left: 20px;
    position: relative;
    z-index: 1;
}

.header .j-selectbox__select:focus {
    outline: 0;
}

.header__search-title {
    display: none;
}

.header--search-open .header__search-title {
    display: block;
    font-size: 18px;
    color: #fff;
    position: absolute;
    top: 0px;
    left: 0;
    width: 100%;
    text-align: center;
    height: 60px;
    line-height: 60px;
    background-color: #006937;
    z-index: 9999;
    overflow: hidden;
}

.header--search-open .header__mobile-search-close {
    position: absolute;
    width: 60px;
    height: 60px;
    top: 0;
    right: 0;
}

.header--search-open .header__mobile-search-close:before {
    content: '';
    position: absolute;
    right: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
    top: 27px;
}

.header--search-open .header__mobile-search-close:after {
    content: '';
    position: absolute;
    right: 20px;
    width: 28px;
    height: 4px;
    background: #fff;
    display: block;
    -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
    top: 27px;
}


.header__search-bar {
    max-width: 1280px;
    margin: 0 auto;
}

#homenew .header__search-bar {
    position: absolute;
    width: 100%;
    bottom: -67px;
    z-index: 0;
}

.header--search-open .header__search-bar {
    position: fixed !important;
    display: block;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
    -webkit-transform: translate3d(0, 0, 0);
    z-index: 100 !important;
    background: #ecebe8;
}

.header--search-open .header__search-bar:before {
    position: fixed;
    background: #ecebe8;
    top: 0;
    left: 0;
    right: 0;
    bottom: -1000px;
    content: '';
    z-index: 0;
    -webkit-transform: translate3d(0, 0, 0);
}

.header__search-bar-list {
    margin: 0;
    padding: 0;
    position: relative;
    background: #ecebe8;
    height: 41px;
}

.header--search-open .header__search-bar-list {
    display: block;
    width: 100%;
    height: 100%;
    top: 60px;
    left: 0;
    z-index: 1;
    box-sizing: border-box;
    position: relative;
}

.header__search-bar-item {
    list-style: none;
    display: block;
    box-sizing: border-box;
    position: relative;
}

.header__search-bar-item--search {
    width: 100%;
}

.header__search-bar-item--category,
.header__search-bar-item--location,
.header__search-bar-item--submit {
    display: none;
}

.header__search-bar-item--category .j-selectbox__inner-wrapper {
    position: relative;
    top: 0px;
}

.header__search-bar-item--category .j-selectbox__ul {
    width: 100%;
    height: 100%;
}

.header__search-bar-item--category .j-selectbox__ul > .j-selectbox__li:first-child {
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 6;
}

.header__search-bar-item--category .j-selectbox__ul > .j-selectbox__li:first-child .j-selectbox__text {
    background-color: #fff;
}

.header__search-bar-item--category .j-selectbox__dropdown-text {
    padding-left: 46px;
    width: 68px;
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 7;
    text-indent: -99999px;
}

.header__search-bar-item--keyword input.header__search-input {
    padding-left: 46px;
    border-radius: 0;
}

.header__search-bar-item--saved-search .header__search-input-wrapper {
    padding-right: 65px;
}

.header__search-bar-item--location {
    padding: 0 20px;
}

.header__search-bar-item--location .j-selectbox__ul {
    overflow: hidden;
}

.header__search-bar-item--submit--loading .header__search-button-icon--search {
    display: none;
}

.header--search-open .header__search-bar-item--keyword,
.header--search-open .header__search-bar-item--category,
.header--search-open .header__search-bar-item--location,
.header--search-open .header__search-bar-item--submit {
    display: block;
    position: relative;
    overflow: hidden;
    z-index: 7;
}

.header--search-open .header__search-bar-item--category {
    position: static;
}

.header--search-open .header__search-bar-item--category .j-selectbox__wrapper:after {
    top: 23px;
    left: 48px;
    z-index: 7;
}

.header--search-open .header__search-location-radius .j-selectbox__wrapper:after {
    top: 34px;
    z-index: 7;
}

.header--search-open .header__search-bar-item--category .j-selectbox__dropdown-text {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
}

.header--search-open .header__search-bar-item--category .j-selectbox__dropdown-text:after {
    content: '';
    width: 5px;
    height: 55px;
    background: #fff;
    position: absolute;
    top: 0px;
    left: 68px;
    border-left: 1px solid #d4d4d4;
    pointer-events: none;
}

.header--search-open .header__search-bar-item--submit {
    padding: 0 20px 20px 20px;
}

.header--search-open .header__search-bar-item--keyword {
    padding-left: 68px;
}

.header--search-open .header__search-bar-item--keyword .j-selectbox__dropdown-text {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}

.header--search-open .header__search-bar-item--keyword .header__suggestion-box-list {
    margin-left: -68px;
    height: auto !important;
    overflow: hidden;
}

.header--search-open .header__search-bar-item--location .header__search-select {
    position: absolute;
    top: 0;
}

.header--search-open .header__search-bar-item--location .j-selectbox__ul {
    margin-top: 6px;
    /*margin-top: 120px;*/
}

.header--search-open .header__search-bar-item--location .header__suggestion-box-list {
    margin-right: -110px;
}

.header--search-open .header__search-bar-item--location .j-selectbox__dropdown-text {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
    width: 110px;
}

.header--search-open .header__search-bar-item--location input.header__search-input {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
}

.header__search-input-wrapper {
    position: relative;
}

.header__search-input-wrapper:after {
    content: '';
    width: 8px;
    height: 55px;
    pointer-events: none;
    background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0) 0%, #fff 100%);
    background: linear-gradient(to right, rgba(255, 255, 255, 0) 0%, #fff 100%);
    top: 0;
    right: 18px;
    position: absolute;
    z-index: 1;
}

.header .header__search-input {

    width: 100%;
    height: 55px;
    background: #fff;
    border: 0;
    font-size: 14px;
    box-sizing: border-box;
    margin: 0 0 10px 0;
    border-radius: 3px;

    padding: 0 18px;
    position: relative;
    z-index: 1;
    -webkit-appearance: none;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
}


.header .header__search-input:focus {

    outline: 1px solid #5d9919;
    outline-offset: 0;
    z-index: 2;
}

.header--keyword input.header__search-input {
    border-radius: 0;
    padding-left: 46px;
}

.header--keyword input.header__search-input::-webkit-input-placeholder {
    color: #a3a3a3;
}

.header--keyword input.header__search-input::-moz-placeholder {
    color: #a3a3a3;
}

.header--keyword input.header__search-input:-ms-input-placeholder {
    color: #a3a3a3;
}

.header--keyword input.header__search-input::placeholder {
    color: #a3a3a3;
}

.header--saved-search .header__search-input-wrapper {
    padding-right: 65px;
}

.header__search-bar-item--location.savedsearch--visible,
.header__search-bar-item--submit.savedsearch--visible {
    display: none;
}

.header select.header__search-select {
    display: none !important;
}

.header__search-select {
    width: 100%;
    height: 55px;
    line-height: 55px;
    background: #fff;
    font-size: 14px;
    box-sizing: border-box;
    cursor: pointer;
    margin: 0 0 10px 0;
    border-radius: 3px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    padding: 0 18px;
}

.header__search-button {
    width: 100%;
    height: 100%;
    background-color: #006937;
    color: #fff;
    border: 0;
    box-sizing: border-box;
    cursor: pointer;
    height: 55px;
    border-radius: 3px;
    outline: 0;
}

.header__search-button-text {
    line-height: 55px;
    font-size: 14px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    display: inline-block;
    vertical-align: middle;
}

.header__search-button-icon {
    width: 19px;
    height: 19px;
    fill: #fff;
    margin: 0 6px 0 0;
    vertical-align: middle;
    display: inline-block;
    margin-top: -3px;
}

.header__search-location {
    width: 100%;
    position: relative;
    box-sizing: border-box;
    padding: 0 110px 0 0;
}

.header__search-location:before {
    content: '';
    position: absolute;
    top: 0;
    right: 110px;
    width: 5px;
    background: #fff;
    height: 55px;
    z-index: 2;
    pointer-events: none;
}

.header__search-location:after {
    content: '';
    position: absolute;
    top: 18px;
    right: 110px;
    width: 1px;
    height: 16px;
    background: #d4d4d4;
    z-index: 3;
}

.header__search-location input.header__search-input {
    padding-left: 49px;
}

.header__search-keyword-icon,
.header__search-location-icon {
    position: absolute;
    width: 16px;
    height: 16px;
    left: 16px;
    top: 19px;
    fill: #006937;
    pointer-events: none;
    z-index: 3;
}

.header--search-open .header__search-keyword-icon {
    margin-left: 68px;
}

.header__search-location-radius {
    width: 110px;
    float: right;
    margin-top: -65px;
}

.header__saved-search-container {
    position: absolute;
    width: 65px;
    height: 100%;
    top: 0;
    right: 0;
    z-index: 1;
    display: block;
}

.header__saved-search-link {
    width: 100%;
    height: 55px;
    display: block;
    -webkit-transition: background-color 0.2s ease-in, color 0.2s ease-in;
    transition: background-color 0.2s ease-in, color 0.2s ease-in;
    line-height: 55px;
    font-size: 14px;
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
    text-decoration: none;
    color: #006937;
    background-color: #fff;
    padding: 0 0 0 25px;
    box-sizing: border-box;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
}

.header__saved-search-link:hover,
.header__saved-search-link:visited,
.header__saved-search-link:active {
    color: #006937;
}

.savedsearch--visible .header__saved-search-link {
    background-color: #006937;
    color: #fff;
}

.savedsearch--visible .header__saved-search-link .header__saved-search-icon {
    fill: #fff;
}

.header__saved-search-icon {
    fill: #006937;
    width: 17px;
    height: 17px;
    margin: -3px 5px 0 0;
    vertical-align: middle;
}

.header__saved-search-text {
    display: none;
}

.header__suggestion-box-list {
    list-style: none;
    background: #fff;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    margin: -10px 0 10px 0px;
    visibility: hidden;
    -webkit-transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease, -webkit-transform 0.2s ease;
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
    will-change: transform, opacity;
    opacity: 0;
    display: none;
    max-height: 300px;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
}

.header__suggestion-box-list--open {
    display: block;
    visibility: visible;
    opacity: 1;
    -webkit-transform: translateY(0);
            transform: translateY(0);
}

.header__suggestion-box-item {
    width: 100% !important;
}

.header__suggestion-box-item:first-child .header__suggestion-box-item-link {
    padding-top: 15px;
}

.header__suggestion-box-item:last-child .header__suggestion-box-item-link {
    padding-bottom: 15px;
}

.header__suggestion-box-item--highlighted .header__suggestion-box-item-link {
    background-color: #F5F5F3;
}

.header__suggestion-box-item--first-recent-search:before {
    content: 'Recent Searches';
    width: 100%;
    padding: 12px 16px 0px 16px;
    font-size: 12px;
    display: block;
    border-top: 1px solid #d4d4d4;
    margin: 5px 0 0 0;
    box-sizing: border-box;
    color: #b7b7b7;
}

.header__suggestion-box-item--recent-searches-only:before {
    margin: 0;
    border: 0;
}

.header__suggestion-box-item-link {
    text-decoration: none;
    display: block;
    padding: 7.5px 16px 7.5px 16px;
    box-sizing: border-box;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
    cursor: pointer;
}

.header__suggestion-box-item-title {
    font-size: 14px;
    line-height: 18px;
    display: block;
    color: #222;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
}

#search-area-wrp .header__suggestion-box-item-title {
    font-weight: normal;
    font-family: "ProximaNova-Regular", Arial, Helvetica, sans-serif;
}

#search-area-wrp .header__suggestion-box-item-title .c-li-sggstnbx-high {
    font-weight: normal;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
}

.header__suggestion-box-item-category {
    color: #5d9919;
    font-size: 14px;
    line-height: 18px;
    display: block;
}

.header__home-hero {
    display: block;
    position: relative;
    overflow: hidden;
    z-index: 0;
    width: 100%;
    height: 160px;
    margin-top: -1px;
    text-align: center;
    color: #fff;
    background: #162432;
}

.header__home-hero--light img {
    opacity: 0.25;
}

.header__home-hero-image {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    position: relative;
}

.header__home-hero-image img {
    -webkit-transition: opacity 0.8s;
    transition: opacity 0.8s;
    width: 767px;
    height: 160px;
    position: absolute;
    top: 0;
    left: 50%;
    -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
}

.header__home-hero-placement {
    -webkit-transition: opacity 0.8s;
    transition: opacity 0.8s;
    display: block;
    position: absolute;
    top: 50%;
    left: 0;
    z-index: 5;
    -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
    text-align: center;
    width: 100%;
}

.header__home-hero-placement div { /* this is the ad placement */
    text-align: center;
    z-index: 3;
    position: relative;
}

.header__home-hero-placement--hidden {
    opacity: 0;
}

.header__home-hero-placement-close {
    visibility: hidden;
    pointer-events: none;
    position: absolute;
    cursor: pointer;
    top: 0;
    right: 50%;
    margin-right: -529px;
    margin-top: 37px;
    width: 44px;
    height: 44px;
    z-index: 6;
    opacity: 0;
    -webkit-transition: opacity 0.8s ease-in;
    transition: opacity 0.8s ease-in;
}

.header__home-hero-placement-close:before {
    content: '';
    position: absolute;
    right: 8px;
    top: 22px;
    width: 28px;
    height: 2px;
    background: #fff;
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
}

.header__home-hero-placement-close:after {
    content: '';
    position: absolute;
    right: 8px;
    top: 22px;
    width: 28px;
    height: 2px;
    background: #fff;
    -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
}

.header__home-hero-placement-close--visible {
    visibility: visible;
    pointer-events: all;
    opacity: 1;
}

.header__home-hero-text-container {
    position: absolute;
    top: 50%;
    z-index: 2;
    width: 100%;
    -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
    -webkit-transition: opacity 0.8s;
    transition: opacity 0.8s;
}

.header__home-hero-text-container--hidden {
    opacity: 0;
}

.header__home-hero-title {
    font-size: 24px;
    line-height: 36px;
    letter-spacing: 0px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
}

.header__home-hero-subtitle {
    font-size: 16px;
    line-height: 20px;
    letter-spacing: 1px;
    margin-top: 12px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
}

.header__banner-container {
    text-align: center;
}

.header__banner-container .placement--header div {
    -webkit-transition: max-height 0.8s;
    transition: max-height 0.8s;
    padding: 0;
    max-height: 130px;
}

.header__banner-container .placement--header div iframe {
    padding: 15px 0;
}

.header__banner-container--hidden .placement--header div {
    max-height: 0;
}

.header__banner-container iframe {
    opacity: 1;
    -webkit-transition: opacity 0.8s;
    transition: opacity 0.8s;
}

.header__banner-container iframe.iframe-hidden {
    opacity: 0;
}

.header__banner-container-close {
    visibility: hidden;
    pointer-events: none;
    position: absolute;
    cursor: pointer;
    top: 0;
    right: 10px;
    margin-right: 0;
    margin-top: 9px;
    width: 44px;
    height: 44px;
    z-index: 6;
    opacity: 0;
    -webkit-transition: opacity 0.8s ease-in;
    transition: opacity 0.8s ease-in;
}

.header__banner-container-close:before {
    content: '';
    position: absolute;
    right: 18px;
    top: 22px;
    width: 14px;
    height: 1px;
    background: #fff;
    -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
}

.header__banner-container-close:after {
    content: '';
    position: absolute;
    right: 18px;
    top: 22px;
    width: 14px;
    height: 1px;
    background: #fff;
    -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
}

.header__banner-container-close--visible {
    visibility: visible;
    pointer-events: all;
    opacity: 1;
}

/** =========================
    Footer
    ========================= **/

/** Footer **/

.footer {
    float: none;
    width: 100%;
    clear: both;
    padding: 0.5em 0;
    background: #222;
    color: #fff;
}

.footer:before,
.footer:after {
    content: "";
    display: table;
    clear: both;
    line-height: 0;
}

.footer .container {
    min-height: 0;
    padding: 0 20px;
}

.footer__download-app-container {
    float: left;
    clear: both;
    width: 100%;
    padding: 1.5em 0;
}

.footer__download-app-icon {
    display: block;
    color: #fff;
    float: left;
    text-decoration: none;
    position: relative;
}

.footer__download-app-icon:after {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -6px;
    pointer-events: none;
    border-width: 6px;
    border-left-color: #414141;
    top: 34%;
    left: auto;
    right: 0;
}

.footer__download-app-icon--linked:hover,
.footer__download-app-icon--linked:focus,
.footer__download-app-icon--linked:active {
    color: #a3a3a3;
    cursor: pointer;
}

.footer__app-icon-bg {
    float: left;
    display: inline-block;
    padding: 4px;
    width: 40px;
    height: 40px;
    border-radius: 10px;
    background-color: #fff;
    box-sizing: border-box;
}

.footer__app-icon {
    width: 32px;
    height: 32px;
}

.footer__download-app-text {
    display: inline-block;
    padding: 0.2em 0 0 1em;
    width: 75%;
    line-height: 1.2;
}

.footer__download-app-badges {
    display: none;
}

.footer__download-app-badge {
    margin: 0 0 0 1.0em;
}

.footer__footer-links {
    float: left;
    clear: both;
    width: 100%;
    border-top: 1px solid #414141;
    border-bottom: 1px solid #414141;
    text-align: center;
    padding: 1.2em 0;
}

.footer__footer-link-col {
    width: 100%;
    display: block;
    float: left;
    margin: 0;
    padding: 1.5em 0;
}

.footer__footer-link-heading {
    padding: 0 0 1.1em 0;
    font-size: 14px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
}

.footer__footer-item {
    padding: 0.2em 0;
    list-style: none;
}

.footer__footer-link {
    color: #fff;
    text-decoration: none;
}

.footer__footer-link:hover,
.footer__footer-link:focus,
.footer__footer-link:active {
    color: #a3a3a3;
}

.footer__social-links {
    padding: 2.4em 0;
    text-align: center;
    clear: both;
}

.footer__social-item {
    display: inline-block;
    padding: 0 0.2em;
}

.footer__social-item-link {
    display: inline-block;
    height: 40px;
    width: 40px;
    border-radius: 50%;
    color: #fff;
    background-color: transparent;
    text-align: center;
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.footer__social-item-link--facebook:hover,
.footer__social-item-link--facebook:focus,
.footer__social-item-link--facebook:active {
    background-color: #3b5998;
}

.footer__social-item-link--twitter:hover,
.footer__social-item-link--twitter:focus,
.footer__social-item-link--twitter:active {
    background-color: #0098f0;
}

.footer__social-item-link--youtube:hover,
.footer__social-item-link--youtube:focus,
.footer__social-item-link--youtube:active {
    background-color: #c10011;
}

.footer__social-item-link--google-plus:hover,
.footer__social-item-link--google-plus:focus,
.footer__social-item-link--google-plus:active {
    background-color: #d53427;
}

.footer__social-item-link--instagram:hover,
.footer__social-item-link--instagram:focus,
.footer__social-item-link--instagram:active {
    background-color: #b03494;
}

.footer__social-item-link--pinterest:hover,
.footer__social-item-link--pinterest:focus,
.footer__social-item-link--pinterest:active {
    background-color: #c8232c;
}

.footer__social-item-link-icon {
    display: inline-block;
    width: 20px;
    height: 20px;
    margin-top: 9px;
    fill: #fff;
}

.footer__social-item-link-text {
    position: absolute;
    top: -99999em;
    left: -99999em;
}

.footer__copyright {
    padding: 0 0 2.4em 0;
    text-align: center;
}

.footer__copyright-text {
    font-size: 12px;
    color: #a3a3a3;
}

/** =========================
    Display Ads
========================= **/

.placement {
    margin: 0 auto;
    text-align: center;
}

.placement > div {
    padding: 10px 0;
}

.placement--compact {
    padding: 10px 0;
}

.placement--full-width > div {
    width: 100% !important;
}

.placement--full-width [id$="__container__"] iframe {
    width: 100% !important;
}

.placement--no-paddng > div {
    padding: 0;
}

.placement--header {
    background: #000;
}

.placement--header div {
    background: #000;
    padding: 5px 0;
}

.placement--micro-bar {
    padding: 0;
    margin-bottom: 15px;
    background-color: #fff;
    box-shadow: 2px 2px 2px 0px rgba(46, 46, 46, 0.1);
}

.placement--micro-bar div {
    padding: 1px 0;
}

.placement--micro-bar iframe {
    width: 98% !important;
    margin: 0 auto;
}

.criteo-placement iframe {
    width: 100%;
    margin-bottom: 15px;
}

/*Quick Fix for https://jira.corp.ebay.com/browse/GTAU-16504*/

.watchlist {
    outline: 0;
}

.watchlist__text {
    display: inline-block;
    width: 0;
    height: 0;
    text-indent: -9999px;
}

.watchlist__icon {
    width: 17px;
    height: 17px;
    fill: #cccccc;
    -webkit-transition: fill 0.2s ease-in;
    transition: fill 0.2s ease-in;
}

.watchlist:hover .watchlist__icon {
    fill: #222;
}

.watchlist:hover .watchlist__icon--delete {
    fill: #d62829;
}

.watchlist--active .watchlist__icon {
    fill: #5d9919;
}

.watchlist--active:hover .watchlist__icon {
    fill: #416d16;
}

.watchlist__page-header {
    font-size: 25px;
    line-height: 30px;
    text-align: center;
    font-weight: normal;
    margin: 5px 0 0 0;
}

.watchlist__page-text {
    font-size: 14px;
    line-height: 20px;
    text-align: center;
    margin: 4px 0 8px 0;
}

.watchlist__page-no-ads {
    text-align: center;
    margin-bottom: 30px;
    font-size: 16px;
}

/** =========================
    Homepage gallery
========================= **/

.tabbed__tabs-container {
    position: relative;
    width: 100%;
    margin-bottom: 0.6em;
    text-align: center;
}

.tabbed__tabs {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
        -ms-flex-align: center;
            align-items: center;
    width: 540px;
    height: 50px;
    list-style: none;
}

.tabbed__tab {
    margin: 0 2%;
}

.tabbed__tab-control {
    display: inline-block;
    font-size: 14px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    font-weight: 100;
    color: #222;
    text-decoration: none;
    outline: 0;
}

.tabbed__tab-control:active {
    color: #222;
}

.tabbed__tab-control:hover {
    border-bottom: 2px solid #92c100;
}

.tabbed__active-tab,
.tabbed__active-tab:hover,
.tabbed__active-tab:focus,
.tabbed__active-tab:active {
    color: #5d9919;
    border-bottom: 2px solid #5d9919;
}

.tabbed__content-container {
    position: relative;
}

.tabbed__tab-content {
    min-height: 300px;
}

.tabbed__tab-items {
    margin-bottom: 20px;
}

.tabbed__empty-tab {
    padding-top: 30px;
    margin-bottom: 15px;
    text-align: center;
}

.tabbed__empty-tab-icon {
    width: 65px;
    height: 65px;
    margin-bottom: 10px;
    fill: #999;
}

.tabbed__empty-tab-content {
    color: #999;
}

.tabbed__empty-tab-content h3 {
    font-size: 16px;
}

.tabbed__see-all {
    margin-bottom: 50px;
    text-align: center;
}

.tabbed__see-all .button {
    width: 100%;
    box-sizing: border-box;
}

.homepage-gallery {
    margin: 0 auto;
}

.homepage-gallery__block {
    float: left;
    width: 48%;
    height: 155px;
    margin: 0 1% 10px 1%;
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    opacity: 1;
    -webkit-transition: opacity 0.8s, box-shadow 0.5s, background-color 0.5s, -webkit-transform 0.8s;
    transition: opacity 0.8s, box-shadow 0.5s, background-color 0.5s, -webkit-transform 0.8s;
    transition: transform 0.8s, opacity 0.8s, box-shadow 0.5s, background-color 0.5s;
    transition: transform 0.8s, opacity 0.8s, box-shadow 0.5s, background-color 0.5s, -webkit-transform 0.8s;
}

.homepage-gallery__block--initial-state {
    -webkit-transform: translateY(15px);
            transform: translateY(15px);
    opacity: 0.5;
}

.homepage-gallery__mrec-placeholder {
    display: none;
}

.homepage-gallery__mrec {
    margin-bottom: 20px;
}

.homepage-gallery__tab-alerts.tabbed__active-content + .homepage-gallery__mrec {
    display: none;
}

.homepage-gallery__saved-search-title-link {
    display: block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 97%;
    margin-left: 1%;
    margin-bottom: 5px;
    font-size: 16px;
    text-decoration: none;
    color: #222;
}

.homepage-gallery__saved-search-row {
    position: relative;
}

.homepage-gallery__block--see-all {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 2;
    background-color: rgba(0, 0, 0, 0.6);
}

.homepage-gallery__block-see-all-link {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
        -ms-flex-align: center;
            align-items: center;
    color: #fff;
    font-size: 18px;
    text-decoration: none;
    -webkit-transition: all 0.2s ease-in;
    transition: all 0.2s ease-in;
}

.homepage-gallery__block-see-all-link:hover {
    color: #fff;
    background-color: rgba(0, 0, 0, 0.3);
}

.homepage-gallery__saved-searches-see-all .button {
    width: 215px;
    margin-top: 30px;
}

.homepage-gallery__saved-searches-no-results {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
        -ms-flex-align: center;
            align-items: center;
    text-align: center;
    padding: 40px 0;
    margin: 10px 0 25px;
    background-color: #fafafa;
}

.homepage-gallery__upsell .gallery-listing__thumb-container {
    margin-bottom: 12px;
}

.homepage-gallery__upsell .gallery-listing__details {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
}

.homepage-gallery__upsell-description {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 3;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-direction: column;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    text-align: center;
    background-color: rgba(0, 0, 0, 0.6);
    color: #fff;
}

.homepage-gallery__upsell-price {
    text-decoration: none;
    color: #92c100;
}

.homepage-gallery__upsell-duration {
    color: #222;
}

.homepage-gallery__upsell-button-container {
    display: none;
}

.homepage-gallery__upsell-button {
    white-space: nowrap;
    min-width: 138px;
}

.homepage-gallery__upsell-button--proseller {
    padding: 15px 5px;
}

.gallery-listing {
    position: relative;
    overflow: hidden;
    box-sizing: border-box;
    padding: 10px 10px 25px 10px;
    background-color: #fff;
    box-shadow: 2px 2px 2px 0px rgba(46, 46, 46, 0.1);
    cursor: pointer;
    -webkit-transition: opacity 0.8s, box-shadow .3s ease-in, -webkit-transform .5s ease-in;
    transition: opacity 0.8s, box-shadow .3s ease-in, -webkit-transform .5s ease-in;
    transition: transform .5s ease-in, opacity 0.8s, box-shadow .3s ease-in;
    transition: transform .5s ease-in, opacity 0.8s, box-shadow .3s ease-in, -webkit-transform .5s ease-in;
}

.gallery-listing:hover {
    box-shadow: 2px 2px 2px 0 rgba(46, 46, 46, 0.3);
    -webkit-transform: translateY(-2px);
            transform: translateY(-2px);
}

.gallery-listing--highlight {
    background-color: #fafafa;
}

.gallery-listing__thumb-container {
    margin-bottom: 5px;
}

.gallery-listing__thumb-link {
    position: relative;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
        -ms-flex-align: center;
            align-items: center;
    overflow: hidden;
    height: 80px;
    border: 1px solid #d4d4d4;
    box-sizing: border-box;
    background-color: #fafafa;
}

.gallery-listing__thumb,
.gallery-listing__thumb img {
    z-index: 1;
    -webkit-transition: -webkit-transform .3s ease-in;
    transition: -webkit-transform .3s ease-in;
    transition: transform .3s ease-in;
    transition: transform .3s ease-in, -webkit-transform .3s ease-in;
    max-width: 100%;
    max-height: 100%;
    position: static;
    vertical-align: middle;
}

.gallery-listing__image-placeholder {
    height: 175px;
}

.gallery-listing__image-placeholder-icon {
    height: 75px;
}

.gallery-listing__title,
.gallery-listing__price {
    box-sizing: border-box;
    width: 100%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 13px;
}

.gallery-listing__title {
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
}

.gallery-listing__price-value {
    color: #92c100;
}

.gallery-listing .price-type {
    font-size: 12px;
    color: #cccccc;
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
}

.gallery-listing__extras-link {
    display: block;
    text-decoration: none;
}

.gallery-listing__location {
    position: absolute;
    bottom: 5px;
    left: 10px;
    color: #cccccc;
    font-size: 12px;
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
}

.gallery-listing__watchlist {
    position: absolute;
    bottom: 5px;
    right: 10px;
    line-height: 0em;
    text-decoration: none;
}

.gallery-listing__watchlist-icon {
    width: 20px;
    height: 20px;
}

.gallery-listing__banner {
    position: absolute;
    top: -50px;
    left: -50px;
    z-index: 2;
    width: 100px;
    height: 100px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    font-size: 12px;
    text-align: center;
    text-transform: uppercase;
    color: white;
    background-color: #ff6103;
    -webkit-transform: rotate(135deg);
            transform: rotate(135deg);
}

.gallery-listing__banner--urgent {
    background-color: #ff6103;
}

.gallery-listing__banner-text {
    position: absolute;
    top: 5px;
    left: 0;
    right: 0;
    display: inline-block;
    -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
}

.featured-categories {
    background-color: #fff;
    padding-top: 60px;
    padding-bottom: 15px;
    text-align: center;
}

.featured-categories__scroller {
    -webkit-overflow-scrolling: auto;
    overflow-x: hidden;
}

.featured-categories__header {
    margin: 0 auto 45px auto;
}

.featured-categories__header h2 {
    margin: 0 0 10px 0;
    font-size: 32px;
}

.featured-categories__subheader {
    font-size: 16px;
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
}

.featured-categories__items {
    padding: 0;
    margin: 0 auto;
    display: inline-block;
}

.featured-categories__item {
    float: left;
    position: relative;
    width: 50%;
    margin: 0;
    box-sizing: border-box;
}

.featured-categories__item:last-child {
    margin-right: 0;
}

.featured-categories__item-link {
    display: block;
    border: none;
    padding: 0;
    margin: 3%;
    width: 94%;
    color: #fff;
    text-decoration: none;
    position: relative;
    overflow: hidden;
}

.featured-categories__item-link:active,
.featured-categories__item-link:visited {
    color: #fff;
}

.featured-categories__item-link:hover {
    text-decoration: none;
    color: #fff;
}

.featured-categories__item-link:hover img {
    -webkit-transform: scale3d(1.01, 1.01, 1);
            transform: scale3d(1.01, 1.01, 1);
    -webkit-filter: brightness(0.85);
            filter: brightness(0.85);
}

.featured-categories__item-link img {
    display: block;
    border: 0;
    margin: 0;
    max-width: 100%;
    width: 100%;
    height: auto;
    -webkit-filter: brightness(0.7);
            filter: brightness(0.7);
    -webkit-transition: all .2s ease-in;
    transition: all .2s ease-in;
}

.featured-categories__item-description {
    position: absolute;
    width: 100%;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 2;
    margin-top: -27px;
    text-align: center;
}

.featured-categories__item-description h4 {
    font-size: 24px;
    font-weight: lighter;
    line-height: 24px;
    padding: 0;
    margin: 0 0 2px 0;
}

.featured-categories__item-description P {
    padding: 0;
    font-family: "ProximaNova-Light", Arial, Helvetica, sans-serif;
    font-size: 16px;
    margin: 4px 0 0 0;
}

.bx-wrapper .bx-pager,
.bx-wrapper .bx-controls-auto {
    position: absolute;
    bottom: -30px;
    width: 100%;
}

.bx-wrapper .bx-pager .bx-pager-item,
.bx-wrapper .bx-controls-auto .bx-controls-auto-item {
    display: inline-block;
    *zoom: 1;
    *display: inline;
}

.bx-wrapper {
    position: relative;
    margin: 0 auto 60px;
    padding: 0;
    *zoom: 1;
}

.bx-wrapper img {
    max-width: 100%;
    display: block;
}

.bx-wrapper .bx-viewport {
    -webkit-transform: translatez(0);
            transform: translatez(0);
}

.bx-wrapper .bx-pager {
    text-align: center;
    font-size: .85em;
    font-family: Arial;
    font-weight: bold;
    color: #666;
    padding-top: 20px;
}

.bx-wrapper .bx-pager.bx-default-pager a {
    background: #666;
    text-indent: -9999px;
    display: block;
    width: 10px;
    height: 10px;
    margin: 0 5px;
    outline: 0;
    border-radius: 5px;
}

.bx-wrapper .bx-pager.bx-default-pager a:hover {
    background: #000;
}

.bx-wrapper .bx-pager.bx-default-pager a.active {
    background: #000;
}

.bx-wrapper .bx-controls-auto {
    text-align: center;
}

.bx-wrapper .bx-controls-auto .bx-start {
    display: block;
    text-indent: -9999px;
    width: 10px;
    height: 11px;
    outline: 0;
    background: transparent none;
    margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-start:hover {
    background-position: -86px 0;
}

.bx-wrapper .bx-controls-auto .bx-start.active {
    background-position: -86px 0;
}

.bx-wrapper .bx-controls-auto .bx-stop {
    display: block;
    text-indent: -9999px;
    width: 9px;
    height: 11px;
    outline: 0;
    background: transparent none;
    margin: 0 3px;
}

.bx-wrapper .bx-controls-auto .bx-stop:hover {
    background-position: -86px -33px;
}

.bx-wrapper .bx-controls-auto .bx-stop.active {
    background-position: -86px -33px;
}

.bx-wrapper .bx-loading {
    min-height: 50px;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 2000;
}

.bx-wrapper .bx-prev {
    left: 10px;
    background: transparent none;
}

.bx-wrapper .bx-prev:hover {
    background-position: 0 0;
}

.bx-wrapper .bx-next {
    right: 10px;
    background: transparent none;
}

.bx-wrapper .bx-next:hover {
    background-position: -43px 0;
}

.bx-wrapper .bx-caption {
    position: absolute;
    bottom: 0;
    left: 0;
    background: #666;
    background: rgba(80, 80, 80, 0.75);
    width: 100%;
}

.bx-wrapper .bx-caption span {
    color: #fff;
    font-family: Arial;
    display: block;
    font-size: .85em;
    padding: 10px;
}

.bx-wrapper .bx-controls-direction a {
    position: absolute;
    top: 50%;
    margin-top: -16px;
    outline: 0;
    width: 32px;
    height: 32px;
    text-indent: -9999px;
    z-index: 9999;
}

.bx-wrapper .bx-controls-direction a.disabled {
    display: none;
}

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-pager {
    text-align: left;
    width: 80%;
}

.bx-wrapper .bx-controls.bx-has-controls-auto.bx-has-pager .bx-controls-auto {
    right: 0;
    width: 35px;
}

.slider {
    background-color: #92c100;
    padding-top: 10px;
}

.slider__items {
    list-style: none;
    padding: 0;
    margin: 0;
}

.slider__content {
    display: table;
    color: #fff;
    min-height: 230px;
    height: 230px;
    width: 100%;
    background-repeat: no-repeat;
    background-position: right top;
}

.slider__content h3 {
    font-size: 32px;
    font-weight: normal;
    margin: 0 0 0.2em 0;
    padding: 0;
    line-height: 32px;
}

.slider__content p {
    padding: 0;
    margin-top: 0;
    max-width: 380px;
}

.slider__content p:last-child {
    margin-bottom: 0;
}

.slider__content a {
    color: #fff;
}

.slider__content a:hover,
.slider__content a:active,
.slider__content a:focus {
    color: #222;
}

.slider__content .slider__content__col-left {
    display: table-cell;
    vertical-align: middle;
    padding-left: 5%;
    width: 50%;
}

.slider__content .slider__content__col-right {
    display: table-cell;
    vertical-align: middle;
    width: 50%;
}

.slider__content.slider__content--1 {
    background-image: url(../img/au/homepage/banner_bicycle_large.png);
}

.slider__content.slider__content--2 {
    background-image: url(../img/au/homepage/slide-002-community.png);
    background-position: 95% 14px;
}

.slider__content.slider__content--2 .slider__content__col-left {
    width: 40%;
}

.slider__content.slider__content--3 {
    background-image: url(../img/au/homepage/slide-003-security.png);
    background-position: 92% 0;
}

.slider__content.slider__content--3 .slider__content__col-left {
    width: 40%;
}

.slider .bx-wrapper {
    margin-bottom: 0;
}

.slider .bx-controls .bx-prev {
    left: 0px;
    background: none;
    text-indent: inherit;
    text-decoration: none;
    -webkit-transform: scaleX(-1);
            transform: scaleX(-1);
    -webkit-filter: FlipH;
            filter: FlipH;
}

.slider .bx-controls .bx-prev svg {
    width: 32px;
    height: 32px;
    fill: #fff;
}

.slider .bx-controls .bx-prev.disabled {
    display: block;
    opacity: 0.2;
}

.slider .bx-controls .bx-next {
    right: 0px;
    background: none;
    text-indent: inherit;
    text-decoration: none;
}

.slider .bx-controls .bx-next svg {
    width: 32px;
    height: 32px;
    fill: #fff;
}

.slider .bx-controls .bx-next.disabled {
    display: block;
    opacity: 0.25;
}

.slider .bx-controls .bx-pager {
    display: none;
}

.slider .bx-controls .bx-pager.bx-default-pager a {
    background: #fff;
    opacity: 0.7;
}

.slider .bx-controls .bx-pager.bx-default-pager a:hover {
    background: #fff;
    opacity: 1;
}

.slider .bx-controls .bx-pager.bx-default-pager a.active {
    background: #fff;
    opacity: 1;
}

/****************************
 Page
****************************/

.popular-searches {
    text-align: center;
}

.popular-searches__link {
    padding: 0 10px;
}

.popular-searches .c-dark-green {
    padding-top: 30px;
    padding-bottom: 10px;
}

@media (min-width: 415px) and (max-width: 1265px) {

.featured-categories__scroller {
    overflow-x: scroll;
    -webkit-overflow-scrolling: touch;
}

}

@media (min-width: 468px) {

.footer__download-app-text {
    float: left;
    padding: 0.8em 2.0em 0 1.0em;
    width: auto;
}

}

@media (min-width: 480px) {

.c-boxy {
    margin-bottom: 20px;
    padding: 30px;
}

[data-spinner-btn].header__upper-deck-item-link .svg-spinner {
    fill: #fff;
}

.footer__footer-links {
    border-top: none;
    text-align: left;
}

.footer__footer-link-col {
    width: 25%;
}

.featured-categories {
    padding-bottom: 65px;
}

.featured-categories__items {
    width: 1245px;
}

.featured-categories__item {
    width: 300px;
    margin: 0 15px 0 0;
}

.featured-categories__item-link {
    width: 100%;
    margin: 0;
}

}

@media (min-width: 550px) {

.tabbed__tabs {
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    width: 100%;
}

}

@media (min-width: 768px) {

body#srchrslt .header__banner-container,
body#vip .header__banner-container {
    min-height: 120px;
}

.header__logo-link {
    width: 150px;
    height: 120px;
}

.header__logo-image {
    width: 140px;
    left: 9px;
    top: -10px;
}

.header--mobile-menu-open .header__logo-image,
.header--search-open .header__logo-image {
    display: block;
}

.header__upper-deck {
    height: 80px;
}

.header__upper-deck-list {
    padding-right: 20px;
    float: right;
    min-width: 552px;
    box-sizing: border-box;
}

.header__upper-deck-item {
    height: 80px;
    line-height: 80px;
}

.header__upper-deck-item--message-center {
    position: relative;
    top: auto;
    right: auto;
}

.header__upper-deck-item--message-center:after {
    display: block;
    -webkit-transition: opacity 0.2s ease-in;
    transition: opacity 0.2s ease-in;
}

.header__upper-deck-item--help,
.header__upper-deck-item--my-gumtree,
.header__upper-deck-item--sign-in-welcome,
.header__upper-deck-item--post-ad,
.header__upper-deck-item--register,
.header__upper-deck-item--signin,
.header__upper-deck-item--signout {
    display: block;
    float: right;
}

.header__upper-deck-item--register {
    margin-left: -1px;
}

.header__upper-deck-item--post-ad {
    padding: 0 0 0 20px;
}

.header__upper-deck-item--my-gumtree {
    overflow: hidden;
}

.header--my-gumtree-open .header__upper-deck-item--my-gumtree {
    overflow: visible;
    background-color: #5d9919;
    z-index: 2;
}

.header--my-gumtree-open .header__upper-deck-item--my-gumtree:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-bottom-color: #c9e080;
    top: 29px;
    right: 26px;
    border-top-color: transparent !important;
}

.header__upper-deck-item--help:before,
.header__upper-deck-item--my-gumtree:before {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-top-color: #c9e080;
    top: 37px;
    right: 26px;
}

.header__upper-deck-item--mobile-menu-trigger {
    display: none;
}

.header__upper-deck-item--show-desktop {
    display: block;
}

.header__upper-deck-item--hide-desktop {
    display: none;
}

.header--mobile-menu-open .header__upper-deck-item--register,
.header--mobile-menu-open .header__upper-deck-item--signin {
    width: auto;
}

.header--mobile-menu-open .header__upper-deck-item--signout {
    width: auto;
}

.header__upper-deck-item-link {
    border: 0;
    display: inline-block;
    color: #fff;
    height: auto;
    line-height: 80px;
    height: 80px;
}

.header__upper-deck-item-link.sign-in {
    min-width: 94px;
}

.header__upper-deck-item-link.register {
    min-width: 102px;
}

.header__upper-deck-item-link:hover,
.header__upper-deck-item-link:active,
.header__upper-deck-item-link:visited {
    color: #fff;
    text-decoration: none;
}

.header__upper-deck-item-link--sign-out {
    display: none;
}

.header__mobile-post-ad-button {
    display: none;
}

.header__post-ad-button {
    width: 141px;
    margin: 18px 0 0 0;
}

.header__message-center-image {
    width: 40px;
    height: 40px;
}

.header__my-gumtree-trigger {
    display: block;
    text-decoration: none;
    cursor: pointer;
    height: 80px;
    padding: 0 20px 0 26px;
}

.header__my-gumtree-text {
    display: block;
    width: 100%;
    line-height: 15px;
    text-align: center;
    padding: 10px;
    font-family: "ProximaNova-Bold", Arial, Helvetica, sans-serif;
    box-sizing: border-box;
}

.header__my-gumtree-user-text {
    display: block;
}

.header__my-gumtree-list {
    display: block;
    visibility: hidden;
    opacity: 0;
    top: 70px;
    -webkit-transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease, -webkit-transform 0.2s ease;
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
    will-change: transform, opacity;
}

.header--my-gumtree-open .header__my-gumtree-list {
    position: absolute;
    left: -33px;
    padding: 0;
    margin: 0;
    background: #fff;
    z-index: 13;
    border-radius: 3px;
    width: 260px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    visibility: visible;
    opacity: 1;
    -webkit-transform: translateY(0);
            transform: translateY(0);
}

.header--my-gumtree-open .header__my-gumtree-list:after {
    content: '';
    position: absolute;
    border: solid transparent;
    height: 0;
    width: 0;
    margin-left: -7px;
    pointer-events: none;
    border-width: 7px;
    border-bottom-color: #fff;
    top: -14px;
    left: 50%;
    margin-left: -4.5px;
}

.header__my-gumtree-item:first-child {
    border-top: 1px solid #d4d4d4;
}

.header__my-gumtree-item:last-child .header__my-gumtree-link {
    border-bottom: 0;
}

.header__my-gumtree-item:last-child {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    overflow: hidden;
}

.header__my-gumtree-link {
    background: #fafafa;
}

.header__my-gumtree-link:hover {
    background: #F5F5F3;
    color: #222;
}

.header__mobile-menu-container {
    display: none;
}

.header__mobile-menu-trigger {
    display: none;
}

.header__primary-navigation-wrapper {
    z-index: 1;
}

.header__primary-navigation-wrapper:before {
    content: '';
    position: absolute;
    width: 160px;
    height: 100%;
    top: 0;
    left: 0;
    background: #ecebe8;
    z-index: 5;
}

.header__primary-navigation-wrapper:after {
    content: '';
    position: absolute;
    width: 8px;
    height: 100%;
    top: 0;
    left: 160px;
    background: -webkit-linear-gradient(left, #ecebe8 0%, rgba(236, 235, 232, 0) 100%);
    background: linear-gradient(to right, #ecebe8 0%, rgba(236, 235, 232, 0) 100%);
    z-index: 5;
}

.header__primary-navigation-list {
    height: 50px;
    padding: 0 152px 0 170px;
}

.header__primary-navigation-item {
    height: 50px;
    line-height: 50px;
    padding: 0 20px;
}

.header__primary-navigation-item--hidden {
    display: none !important;
}

.header__primary-navigation-item--open {
    background-color: #fff;
}

.header__primary-navigation-item--open:after {
    display: none;
}

.header__primary-navigation-item--open .header__secondary-navigation-list {
    visibility: visible;
    z-index: 9;
    -webkit-transform: translateY(0);
            transform: translateY(0);
    opacity: 1;
}

.header__primary-navigation-item--open:before {
    content: '';
    position: absolute;
    right: -20px;
    left: -20px;
    bottom: 0px;
    top: -20px;
    z-index: 8;
}

.header__primary-navigation-item:after {
    content: '';
    background: #d4d4d4;
    width: 1px;
    height: 12px;
    position: absolute;
    top: 19px;
    right: 0;
}

.header__primary-navigation-item--more-categories {
    border: 0;
    background: #ecebe8;
    padding: 0 15px 0 20px;
    width: auto;
    right: 5px;
    top: 0px;
    z-index: 1;
}

.header__primary-navigation-item--more-categories.header__primary-navigation-item--open {
    background: #fff !important;
}

.header__primary-navigation-item--more-categories.header__primary-navigation-item--open:before {
    display: none;
}

.header__more-categories-small {
    display: none;
}

.header__more-categories-medium-up {
    display: block;
}

.header__secondary-navigation-list {
    visibility: hidden;
    position: absolute;
    background: #fff;
    left: 0;
    width: 485px;
    padding: 10px 10px 5px 10px;
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
    -webkit-columns: 2;
       -moz-columns: 2;
            columns: 2;
    top: 50px;
    opacity: 0;
    -webkit-transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: opacity 0.2s ease, -webkit-transform 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease;
    transition: transform 0.2s ease, opacity 0.2s ease, -webkit-transform 0.2s ease;
    -webkit-transform: translateY(-30px);
            transform: translateY(-30px);
    will-change: transform, opacity;
}

.header__secondary-navigation-list--featured {
    padding-bottom: 200px;
}

.header.header--all-categories-open .header__secondary-navigation-list-wrapper--all-categories {
    position: static;
    width: auto;
    height: auto;
    padding: 0;
}

.header__primary-navigation-mobile-trigger {
    display: none;
}

.header__secondary-navigation-item {
    width: 100%;
    float: left;
}

.header__secondary-navigation-item--is-primary {
    display: none !important;
}

.header__secondary-navigation-item--is-dummy {
    display: block !important;
}

.header__secondary-navigation-link {
    height: 40px;
    line-height: 40px;
    text-align: left;
    padding: 0 10px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}

.header__secondary-navigation-link:hover,
.header__secondary-navigation-link:visited,
.header__secondary-navigation-link:active {
    color: #222;
    background-color: #F5F5F3;
}

.header__secondary-navigation-icon {
    display: none;
}

.header__secondary-navigation-down-icon {
    display: none;
}

.header .j-selectbox__wrapper {
    width: 100%;
    height: 41px;
    position: absolute;
    /*cursor: pointer;*/
    text-align: center;
}

.header .j-selectbox__wrapper:after {
    top: 30px;
    right: 20px;
}

.header .j-selectbox__wrapper--expanded .j-selectbox__dropdown-icon {
    fill: #fff;
}

.header .j-selectbox__ul {
    display: block;
    margin: 0;
    max-height: 600px;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
}

.header .j-selectbox__child-ul .j-selectbox__text {
    padding-left: 80px;
}

.header .j-selectbox__child-ul .j-selectbox__child-ul .j-selectbox__text {
    padding-left: 100px;
}

.header .j-selectbox__child-ul .j-selectbox__li:after {
    left: 60px;
}

.header .j-selectbox__child-ul .j-selectbox__li .j-selectbox__li:first-child:before {
    left: 60px;
}

.header .j-selectbox__child-ul .j-selectbox__li .j-selectbox__li:after {
    left: 80px;
}

.header .j-selectbox__text {
    font-size: 16px;
    padding: 0 30px;
}

.header .j-selectbox__svg-icon {
    width: 19px;
    height: 19px;
    margin: 14px 17px 0 0;
}

.header .j-selectbox__dropdown-icon {
    width: 19px;
    height: 19px;
    top: 24px;
    left: 14px;

}

.header .j-selectbox__down-icon:before {
    right: 24px;
}

.header .j-selectbox__child-ul--is-expanded ~ .j-selectbox__down-icon:before {
    right: 24px;
}

.header .j-selectbox__dropdown-text {
    line-height: 66px;
    height: 100%;
}

.header .j-selectbox__select:focus {
    outline: 1px solid #5d9919;
    z-index: 2;
}

.header--search-open .header__mobile-search-close {
    display: none;
}

.header__search-bar {
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.2);
}

#homenew .header__search-bar {
    z-index: 3;
    bottom: 0;
    left: 50%;
    -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
}

.header__search-bar-list {
    padding: 0 100px;
}

.header--search-open .header__search-bar-list {
    position: static;
    width: auto;
    height: auto;
    padding: 0;
    background: #fff;
}

.header__search-bar-item {
    display: block;
    border-right: 1px solid #d4d4d4;
    float: left;
    height: 100%;
}

.header__search-bar-item--search {
    width: auto;
}

.header__search-bar-item--category,
.header__search-bar-item--location,
.header__search-bar-item--submit {
    display: block;
}

.header__search-bar-item--category {
    background: #fff;
    position: absolute;
    top: 0;
    left: 0;
    width: 100px;
}

.header__search-bar-item--category .j-selectbox__inner-wrapper {
  max-height: 600px;
width: 685px;
z-index: 10;
top: -14px;
left: -6px;

}

.header__search-bar-item--category .j-selectbox__ul {
    padding-top: 50px;
    margin-top: 41px;
}

.header__search-bar-item--category .j-selectbox__dropdown-text {
    padding-left: 20px;
    text-indent: 100%;
    white-space: nowrap;
    overflow: hidden;
    display: none !important;
}

.header__search-bar-item--keyword input.header__search-input {
    padding-left: 18px;
    box-shadow: none;
}

.header__search-bar-item--keyword {
    width: 50%;
}

.header__search-bar-item--location {
    width: 50%;
    position: relative;
    padding: 0 110px 0 0;
    border-right: 0;
}

.header__search-bar-item--submit {
    width: 100px;
    border-right: 0;
    position: absolute;
    top: 0;
    right: 0;
}

.header--search-open .header__search-bar-item--category {
    position: relative;
}

.header--search-open .header__search-bar-item--submit {
    padding: 0;
}

.header--search-open .header__search-bar-item--keyword {
    padding: 0;
}

.header--search-open .header__search-bar-item--keyword .header__suggestion-box-list {
    margin-left: 0;
}

.header--search-open .header__search-bar-item--location .header__search-select {
    position: static;
}

.header .header__search-input {
  height: 42px;
    box-shadow: none;
    border-radius: 0;
    margin: 0;
}

.header .header__search-input:focus {
    box-shadow: none;
}

.header--keyword input.header__search-input {
    font-size: 14px;
    padding: 0 18px;
    border: 0;
    margin: 0;
    border-radius: 0;
    box-shadow: none;
    height: 66px;
}

.header__search-bar-item--location.savedsearch--visible,
.header__search-bar-item--submit.savedsearch--visible {
    display: block;
}

.header__search-select {
    margin: 0;
    border-radius: 0;
    box-shadow: none;
    padding: 0;
    line-height: 66px;
}

.header__search-button {
  height: 41px;
    border-radius: 0;
}

.header__search-button-text {
    display: none;
}

.header__search-button-icon {
    width: 30px;
    height: 26px;
    margin: 0;
}

.header__search-location {
    padding: 0;
}

.header__search-location:before {
    display: none;
}

.header__search-location:after {
    top: 25px;
    right: 0;
    height: 16px;
    z-index: 1;
}

.header__search-keyword-icon,
.header__search-location-icon {
    width: 19px;
    height: 19px;
    left: 20px;
    top: 23px;
}

.header__search-keyword-icon {
    display: none;
}

.header__search-location-radius {
    position: absolute;
    float: none;
    margin: 0;
    top: 0;
    right: 0;
}

.header__saved-search-link {
    height: 66px;
    line-height: 66px;
    box-shadow: none;
}

.header__suggestion-box-list {
    margin: 1px 0 0 0;
    z-index: 10;
    position: relative;
    display: block;
    max-height: 600px;
}

.header__suggestion-box-item--first-recent-search:before {
    padding: 25px 30px 15px 30px;
    color: #222;
}

.header__suggestion-box-item + .header__suggestion-box-item--first-recent-search {
    margin: 15px 0 0 0;
}

.header__suggestion-box-item-link {
    padding: 7.5px 30px 7.5px 30px;
}

.header__suggestion-box-item-link:hover {
    background-color: #F5F5F3;
    color: #5d9919;
}

.header__suggestion-box-item-title {
    font-size: 16px;
    line-height: 20px;
}

.header__suggestion-box-item-title strong {
    font-weight: normal;
    font-family: "ProximaNova-Regular", Arial, Helvetica, sans-serif;
}

.header__home-hero {
    height: 320px;
}

.header__home-hero-image {
    box-sizing: border-box;
    height: 100%;
}

.header__home-hero-image img {
    width: 1279px;
    height: auto;
    margin-top: -70px;
}

.header__home-hero-placement {
    padding-bottom: 66px;
}

.header__home-hero--cars .header__home-hero-placement {
    padding-bottom: 84px;
}

.header__home-hero-text-container {
    margin-top: -30px;
}

.header__home-hero-title {
    font-size: 48px;
    line-height: 48px;
    letter-spacing: -1px;
}

.header__banner-container-close {
    right: 50%;
    margin-right: -399px;
}

.footer__download-app-badges {
    float: right;
    display: block;
}

.footer__social-links {
    float: left;
    margin-left: -1.2em;
    padding-top: 1.7em;
}

.footer__copyright {
    float: right;
    padding-top: 2.4em;
    text-align: left;
}

.placement > div {
    padding: 20px 0;
}

.placement--compact {
    padding: 0;
}

.placement--header div {
    padding: 20px 0;
}

.watchlist__icon {
    width: 20px;
    height: 20px;
}

.watchlist__page-header {
    font-size: 35px;
    line-height: 50px;
    margin: 30px 0 0 0;
}

.watchlist__page-text {
    margin: 4px 0 30px 0;
}

.tabbed__tabs-container {
    margin-bottom: 20px;
}

.tabbed__tab {
    margin: 0 3%;
}

.tabbed__tab-control {
    font-size: 16px;
}

.tabbed__tab-content {
    min-height: 450px;
}

.tabbed__tab-items {
    margin-bottom: 40px;
}

.tabbed__empty-tab {
    min-height: 400px;
    padding-right: 312px;
}

.tabbed__empty-tab-content h3 {
    font-size: 23px;
}

.tabbed__see-all {
    margin-bottom: 60px;
}

.tabbed__see-all .button {
    width: 175px;
}

.homepage-gallery__block {
    width: 300px;
    height: 250px;
    margin: 0 7px 15px 7px;
}

.homepage-gallery__mrec-placeholder {
    display: block;
}

.homepage-gallery__mrec {
    position: absolute;
    right: 7px;
    top: 0;
    padding: 0;
    margin: 0;
}

.homepage-gallery__mrec div {
    padding: 0;
    margin: 0;
}

.homepage-gallery__tab-alerts .tabbed__empty-tab {
    padding-right: 0;
}

.homepage-gallery__upsell-details {
    float: left;
}

.homepage-gallery__upsell-button-container {
    float: right;
    display: block;
}

.gallery-listing__thumb-container {
    margin-bottom: 13px;
}

.gallery-listing__thumb-link {
    height: 175px;
}

.gallery-listing__title {
    padding-right: 30px;
    font-size: 16px;
    font-weight: normal;
}

.gallery-listing__price {
    position: absolute;
    top: 115px;
    left: 0;
    z-index: 3;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
        -ms-flex-direction: column;
            flex-direction: column;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
        -ms-flex-pack: center;
            justify-content: center;
    box-sizing: border-box;
    width: auto;
    height: 45px;
    padding: 0.8em 0.8em 0.67em 1.2em;
    border-radius: 0 3px 3px 0;
    background-color: rgba(0, 0, 0, 0.7);
}

.gallery-listing__price-value {
    color: #fff;
    font-size: 22px;
}

.gallery-listing .price-currency,
.gallery-listing__price-currency {
    float: left;
    margin-right: 2px;
    color: #92c100;
    font-size: 13px;
    line-height: 1em;
}

.gallery-listing .price-type {
    display: block;
    margin-left: 8px;
    line-height: 1em;
    color: #999;
}

.gallery-listing .price-type--no-amount {
    display: inline-block;
    margin-left: 0;
    font-size: 20px;
    color: #fff;
}

.gallery-listing__location {
    bottom: 12px;
    font-size: 14px;
}

.gallery-listing__watchlist {
    right: 13px;
    bottom: 13px;
}

}

@media (min-width: 768px) and (max-width: 1023px) {

.homepage-gallery {
    width: 628px;
}

}

@media (min-width: 970px) {

.header__home-hero {
    height: 450px;
}

}

@media (min-width: 1024px) {

.header__logo-link {
    width: 170px;
    height: 130px;
}

.header__logo-image {
    left: 20px;
}

.header__post-ad-button:hover {
    background-color: #ce4d00;
}

.header__primary-navigation-item--more-categories {
    padding-right: 20px;
    right: 5px;
}

 .header__search-bar {
    /*padding: 20px;
    background-color: rgba(0, 0, 0, 0.5);
    */
    padding: 6px;
    /* background-color: rgba(0, 0, 0, 0.34); */
    box-sizing: border-box;
    border-radius: 4px;
}

.header__search-bar-item--keyword {
    width: 60%;
}

.header__search-bar-item--saved-search .header__search-input-wrapper {
    padding-right: 150px;
}

.header__search-bar-item--location {
    width: 40%;
}

.header .header__search-input:focus {
    outline: 1px solid #5d9919;
    outline-offset: 0;
    z-index: 2;
}

.header--saved-search .header__search-input-wrapper {
    padding-right: 150px;
}

.header__search-button {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header__search-button:hover {
    background-color: #426d12;
}

.header__saved-search-container {
    width: 150px;
}

.header__saved-search-text {
    display: inline-block;
}

.header__home-hero-image {
    margin-top: 0;
    height: auto;
}

.header__home-hero-image img {
    margin-top: 0;
}

.header__home-hero-placement {
    padding-bottom: 106px;
}

.header__home-hero-text-container {
    margin-top: -53px;
}

.header__banner-container-close {
    margin-right: -409px;
}

.footer .container {
    max-width: 1280px;
}

}

@media (min-width: 1024px) and (max-width: 1279px) {

.homepage-gallery {
    width: 942px;
}

.homepage-gallery__block--saved-search:nth-child(n+4) {
    display: none;
}

}

@media (min-width: 1280px) {

a:hover,
a:active,
a:focus {
    color: #92c100;
}

.header__upper-deck-list {
    z-index: 30;
}

.header__upper-deck-item--message-center,
.header__upper-deck-item--my-gumtree,
.header__upper-deck-item--register,
.header__upper-deck-item--signin {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header__upper-deck-item--message-center:hover,
.header__upper-deck-item--my-gumtree:hover,
.header__upper-deck-item--register:hover,
.header__upper-deck-item--signin:hover {
    z-index: 1;
    background-color: #5d9919;
}

.header__upper-deck-item--message-center:hover:after,
.header__upper-deck-item--my-gumtree:hover:after,
.header__upper-deck-item--register:hover:after,
.header__upper-deck-item--signin:hover:after {
    opacity: 0;
}

.header__upper-deck-item--my-gumtree:hover .header__my-gumtree-trigger:after {
    opacity: 1;
}

.header__my-gumtree-user-name:hover {
    text-decoration: underline;
}

.header__primary-navigation-wrapper {
    -ms-touch-action: auto;
        touch-action: auto;
    -webkit-user-select: all;
       -moz-user-select: all;
        -ms-user-select: all;
            user-select: all;
    -webkit-text-size-adjust: auto;
        -ms-text-size-adjust: auto;
            text-size-adjust: auto;
    z-index: 20;
}

.header__primary-navigation-wrapper:after {
    display: none;
}

.header__primary-navigation-list {
    overflow: visible;
    -webkit-overflow-scrolling: none;
    position: relative;
    -webkit-transform: none;
            transform: none;
    padding-left: 200px;
}

.header__primary-navigation-item {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header__primary-navigation-item:hover {
    color: #5d9919;
    background-color: #fff;
}

.header__primary-navigation-item:hover:after {
    opacity: 0;
}

.header__primary-navigation-item--hover-removed:hover {
    color: #222;
    background-color: transparent;
}

.header__primary-navigation-item--hover-removed:hover:after {
    opacity: 1;
}

.header__primary-navigation-item--desktop-hidden {
    display: none !important;
}

.header__primary-navigation-item:after {
    -webkit-transition: opacity 0.1s ease-in;
    transition: opacity 0.1s ease-in;
    opacity: 1;
}

.header__primary-navigation-item--more-categories:before {
    display: none;
}

.header__primary-navigation-item--more-categories {
    z-index: 20;
}

.header__primary-navigation-link {
    z-index: 10;
    position: relative;
}

.header__secondary-navigation-list:after {
    content: '';
    position: absolute;
    top: 0;
    left: -30px;
    right: -30px;
    bottom: -50px;
    z-index: 0;
    cursor: default;
}

.header__secondary-navigation-item--feature {
    top: 220px;
}

.header .j-selectbox__wrapper {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header .j-selectbox__wrapper:hover {
    background-color: #F5F5F3;
}

.header .j-selectbox__text:hover {
    background-color: #F5F5F3;
}

.header .j-selectbox__down-icon:hover:after {
    background-color: #F5F5F3;
}

.header .j-selectbox__dropdown-text {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header .j-selectbox__dropdown-text:hover {
    background-color: #f9f9f9;
}

#homenew .header__search-bar {
    padding: 30px 50px;
    box-shadow: none;
}

.header__search-bar-item--category .j-selectbox__ul > .j-selectbox__li:first-child .j-selectbox__text:hover {
    background-color: #F5F5F3;
}

.header__search-bar-item--keyword input.header__search-input {
    font-size: 24px;
    padding-left: 30px;
}

.header__search-input-wrapper:after {
    display: none;
}

.header .header__search-input {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header .header__search-input:hover {
    background-color: #F5F5F3;
}

.header--keyword input.header__search-input {
    -webkit-transition: background-color 0.2s ease-in;
    transition: background-color 0.2s ease-in;
}

.header--keyword input.header__search-input:hover {
    background-color: #F5F5F3;
}

.header__saved-search-link:hover {
    background-color: #F5F5F3;
}

.header__home-hero {
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);
}

.header__home-hero-image {
    box-sizing: content-box;
    padding: 0;
}

.header__home-hero-image img {
    width: 2000px;
}

.header__home-hero-placement {
    padding-bottom: 126px;
}

.header__home-hero-placement-close--visible {
    opacity: 0.7;
    -webkit-transition: opacity 0.2s ease-in, background-color 0.2s ease-in;
    transition: opacity 0.2s ease-in, background-color 0.2s ease-in;
}

.header__home-hero-placement-close--visible:hover {
    opacity: 1;
    background-color: rgba(0, 0, 0, 0.2);
}

.header__home-hero-text-container {
    margin-top: -63px;
}

.header__banner-container-close:hover {
    opacity: 0;
}

.header__banner-container-close--visible {
    opacity: 0.7;
    -webkit-transition: opacity 0.2s ease-in, background-color 0.2s ease-in;
    transition: opacity 0.2s ease-in, background-color 0.2s ease-in;
}

.header__banner-container-close--visible:hover {
    opacity: 1;
    background-color: rgba(1, 1, 1, 0.1);
}

.watchlist__page-header {
    margin: 68px 0 0 0;
}

.watchlist__page-text {
    margin: 4px 0 63px 0;
}

.tabbed__empty-tab {
    padding-right: 0;
}

.homepage-gallery {
    width: 1256px;
}

}
.icon_search_submit{
  top: 11px !important;
  width: 72px !important;
  font-size: 1.5em !important;
}
@media (max-width: 1200px) {


.icon_search_submit{
  top: 11px !important;
  width: 72px !important;
  font-size: 1.5em !important;
}
.slider .slider__content {
    min-height: 190px;
    height: 190px;
    background-size: 50% auto;
}

.slider .slider__content.slider__content--3 {
    background-size: 40% auto;
}

}

@media (max-width: 900px) {

.slider .slider__content {
    min-height: 150px;
    height: 150px;
}

}

@media (max-width: 768px) {

.c-show-tablet {
    display: block;
}

}

@media (max-width: 767px) {

.homepage-gallery__block--saved-search:nth-child(n+3) {
    display: none;
}

.slider .slider__content {
    min-height: 150px;
    height: 150px;
}

}

@media (max-width: 727px) {

#div-gpt-ad-632115744089839813-leaderboard-footer,
#div-gpt-ad-632115744089839813-leaderboard-footer2 {
    max-height: 70px;
    overflow: hidden;
}

}

@media (max-width: 660px) {

.slider .slider__content {
    display: block;
    min-height: 290px;
    background-size: 80% auto;
    background-position: center 10%;
    text-align: center;
}

.slider .slider__content.slider__content--3 {
    background-size: 55% auto;
}

.slider .slider__content .slider__content__col-left {
    width: 100%;
    display: block;
    box-sizing: border-box;
    padding: 140px 0 0 0;
    min-height: 290px;
    background: -webkit-linear-gradient(top, rgba(146, 193, 0, 0) 40%, #92c100 50%, #92c100 100%);
    background: linear-gradient(to bottom, rgba(146, 193, 0, 0) 40%, #92c100 50%, #92c100 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0092c100', endColorstr='#92c100', GradientType=0); /* IE6-9 */
}

.slider .slider__content .slider__content__col-right {
    display: none;
}

.slider .bx-controls .bx-pager {
    display: block;
    bottom: 20px;
}

.slider .bx-controls .bx-next,
.slider .bx-controls .bx-prev {
    display: none !important;
}

}

@media (max-width: 600px) {

.c-block-mobile {
    display: block;
}

.c-show-tablet {
    display: none;
}

.c-hide-mobile {
    display: none;
}

.c-show-mobile {
    display: block;
}

.c-pull-none-mobile {
    float: none;
}

}

@media (max-width: 479px) {

.tabbed__tabs-container {
    overflow-x: scroll;
    -webkit-overflow-scrolling: touch;
}

}

@media (max-width: 400px) {

.slider .slider__content {
    background-size: 290px auto;
}

}

@keyframes spinnerRotate {

from {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
}

to {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
}

}

@-webkit-keyframes spinnerRotate {

from {
    -webkit-transform: rotate(0deg);
}

to {
    -webkit-transform: rotate(360deg);
}

}

.css_adv_menu {
    font-size: 1.5em;
    height: 29px !important;
    width: 84px !important;
    top: 10px !important;
    left: 0 !important;
    cursor: pointer;
}

.txt_search{
  max-width: 650px !important;
  font-size:15px !important;
}
.icon_search{
  display: none; !important;
  font-size:1.4em !important;
  width: 19px;
  height: 19px;
  top: 19px;
  left: 14px !important;
}

.header--search-open .icon_search {
    margin-left: 68px !important;
}
.header--search-open .icon_search_submit {
  font-size: 1.3em !important;
}
.header--search-open #categoryId-wrp {
  top: 0 !important;
  position: absolute !important;
}
.header--search-open .css_adv_menu {
    top: none !important;
    left: 10px !important;
    height: 36px !important;
    width: 50px !important;
}
.header--search-open .search-query-wrp {
  border-left: 1px solid #d4d4d4;
  padding-left: 10px;
  height: 64px;
  border-left: 1px solid #d4d4d4;
}
.header--search-open #search-query-wrp {
  border-left: 1px solid #d4d4d4;
  padding-left: 5px;
}

.header--search-open  #txt_searchbox{
  top: 5px;
  box-shadow: none;
  width: 98.4% !important;
}
.header--search-open .j-selectbox__wrapper{
  margin-top: 10px;
}

.header--search-open .j-selectbox__dropdown-icon{
  top: 5px;
}

.header--search-open #header__search-bar-item{
      padding-bottom: 80px;
      /*padding-right: 8%;*/
}
.header--search-open #srch-radius-input{
      top: 10px;
}
.header--search-open #header__search-location-radius{
  padding-top: 110px;
}
.header--search-open #map_marker{
  font-size: 1.4em;
position: absolute;
width: 16px;
height: 16px;
left: 16px;
top: 19px;
fill: #5d9919;
pointer-events: none;
z-index: 3;
}

.header--search-open #srch-radius-wrpwrapper_location {
    padding-top: 49px;
}
.header--search-open #srch-radius-wrpwrapper {
  margin-top: -49px;
}

@media (max-width: 766px){
  .icon_search{
    display: inline;;
    font-size:1.4em !important;
    width: 19px;
    height: 19px;
    top: 19px;
    left: 14px !important;
  }

  .header .j-selectbox__dropdown-icon {
      width: 16px;
      height: 16px;
      fill: #5d9919;
      position: absolute;
      top: 19px;
      left: 16px;
      /*left: 32%;*/
      z-index: 7;
  }

}
@media (max-width: 1026px) {
		.header__search-bar-list {
			    height: 17px !important;
		}
    .pull-left{
			position: absolute;
	    margin: auto !important;
	    top: 0;
	    right: 0;
	    left: 0;
	    width: 182px ;
    }
		.jm-module-raw {
			padding-top: 7% !important;
			z-index: 999;
			width: 100%;
      padding-bottom: 8px;

		}
		.owl-nav{
			display: none;
		}

		.txt_search{
		      font-size:14px !important;
		}


		.header .j-selectbox__wrapperTK:after {
		    content: '';
		    position: absolute;
		    border: solid transparent;
		    height: 0;
		    width: 0;
		    margin-left: -7px;
		    pointer-events: none;
		    border-width: 7px;
		    border-top-color: #b7b7b7;
		    top: 34px;
		    right: 42px;
		    z-index: 2;
		}

}
