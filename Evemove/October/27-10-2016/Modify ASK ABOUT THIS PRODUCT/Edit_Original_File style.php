<?php
/**
 * @version $Id: default_contact.php 370 2015-01-07 11:34:16Z michal $
 * @package DJ-Catalog2
 * @copyright Copyright (C) 2012 DJ-Extensions.com LTD, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Michal Olczyk - michal.olczyk@design-joomla.eu
 *
 * DJ-Catalog2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-Catalog2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-Catalog2. If not, see <http://www.gnu.org/licenses/>.
 *
 */

defined ('_JEXEC') or die('Restricted access');

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.tooltip');

if (isset($this->error)) { ?>
    <div class="djc_contact-error">
        <?php echo $this->error; ?>
    </div>
<?php } ?>

<div class="djc_contact_form">
    <form id="djc_contact_form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate">
        <fieldset>
            <div class="panel panel-default" style="    border-radius: 0px !important;">
                <div class="panel-heading"><h4><?php echo JText::_('COM_DJCATALOG2_FORM_LABEL'); ?>&nbsp;&nbsp;<i class="fa fa-question-circle" aria-hidden="true"></i></h4></div>
                <div class="panel-body">
                    
                
                    <div class="control-group">
                        <!--Section contact_name -->
                                    <label class="col-sm-1 ">
                                    <label for="subject" class="col-sm-4 control-label">
                                            <div class="clr mandatory oh">
                                                    <?php echo $this->contactform->getLabel('contact_name'); ?>
                                            </div>
                                    </label>
                                    </label>
                        <div class="col-sm-5">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-10">
                                    <?php echo $this->contactform->getInput('contact_name'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <!--Section  contact_email -->
                        <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-2 control-label">
                                    <div class="clr mandatory oh">
                                        <?php echo $this->contactform->getLabel('contact_email'); ?>
                                    </div>
                            </label>
                        </label>
                            <div class="col-sm-5">
                                    <div class="input-group col-lg-12">
                                        <div class="input-append col-lg-12">
                                            <?php echo $this->contactform->getInput('contact_email'); ?>
                                        </div>
                                    </div>
                        </div>
                        <div class="clear"></div>
                    </div>


                
                <?php if ((int)$this->params->get('contact_company_name_field', '0') > 0) { ?>

                    <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_company_name'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_company_name'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>
                <?php if ((int)$this->params->get('contact_street_field', '0') > 0) { ?>

                    <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_street'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_street'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>
                <?php if ((int)$this->params->get('contact_city_field', '0') > 0) { ?>

                    <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_city'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_city'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>
                <?php if ((int)$this->params->get('contact_zip_field', '0') > 0) { ?>


                    <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_zip'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_zip'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>
                <?php if ((int)$this->params->get('contact_country_field', '0') > 0) { ?>

                    <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_country'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_country'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>
                <?php if ((int)$this->params->get('contact_phone_field', '0') > 0) { ?>

                        <div class="control-group">
                        <!--Section contact_subject -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            <?php echo $this->contactform->getLabel('contact_phone'); ?>
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-12">
                                <div class="input-append col-lg-12">
                                    <?php echo $this->contactform->getInput('contact_phone'); ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>

                <div class="control-group">
                    <!--Section contact_subject -->
                        <label class="col-sm-1 ">
                        <label for="subject" class="col-sm-4 control-label">
                                <div class="clr mandatory oh">
                                        <?php echo $this->contactform->getLabel('contact_subject'); ?>
                                </div>
                        </label>
                        </label>
                    <div class="col-sm-11">
                        <div class="input-group col-lg-12">
                            <div class="input-append col-lg-12">
                                <?php echo $this->contactform->getInput('contact_subject'); ?>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="control-group">
                    <!--Section contact_message -->
                        <label class="col-sm-1 ">
                        <label for="subject" class="col-sm-4 control-label">
                                <div class="clr mandatory oh">
                                        <?php echo $this->contactform->getLabel('contact_message'); ?>
                                </div>
                        </label>
                        </label>
                    <div class="col-sm-11">
                        <div class="input-group col-lg-12">
                            <div class="input-append col-lg-12">
                                <?php echo $this->contactform->getInput('contact_message'); ?>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                </div>

                
                <?php if ((int)$this->params->get('contact_email_copy_field', '1') > 0) { ?>

                    <div class="control-group">
                        <!--Section contact_email_copy -->
                            <label class="col-sm-1 ">
                            <label for="subject" class="col-sm-4 control-label">
                                    <div class="clr mandatory oh">
                                            
                                    </div>
                            </label>
                            </label>
                        <div class="col-sm-11">
                            <div class="input-group col-lg-3">
                                <div class="input-append col-lg-3">
                                     <label><?php echo $this->contactform->getInput('contact_email_copy'); ?></label>
                                    <div class="clear"></div>
                                </div>
                                <?php echo $this->contactform->getLabel('contact_email_copy'); ?>
                            </div>
                        </div>
                    </div>
                
                <?php } ?>



            <?php //Dynamically load any additional fields from plugins. ?>
                 <?php foreach ($this->contactform->getFieldsets() as $fieldset): ?>
                      <?php if ($fieldset->name != 'contact'):?>
                           <?php $fields = $this->contactform->getFieldset($fieldset->name);?>
                           <?php foreach($fields as $field): ?>
                                <?php if ($field->hidden): ?>
                                     <?php echo $field->input;?>
                                <?php else:?>
                                    <div class="control-group">
                                     <div class="control-label">
                                        <?php echo $field->label; ?>
                                        <?php if (!$field->required && $field->type != "Spacer"): ?>
                                           <span class="optional"><?php echo JText::_('COM_DJCATALOG2_OPTIONAL');?></span>
                                        <?php endif; ?>
                                     </div>
                                     <div class="controls"><?php echo $field->input;?></div>
                                     </div>
                                <?php endif;?>
                           <?php endforeach;?>
                      <?php endif ?>
                 <?php endforeach;?>
            

                <div class="controls-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="margin-left: 14px;">
                        <button class="btn button validate" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('COM_DJCATALOG2_CONTACT_SEND'); ?></button>
                        <button id="djc_contact_form_button_close" class="btn button"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo JText::_('COM_DJCATALOG2_CONTACT_FORM_CLOSE')?></button>
                        <input type="hidden" name="option" value="com_djcatalog2" />
                        <input type="hidden" name="task" value="item.contact" />
                        <input type="hidden" name="id" value="<?php echo $this->item->slug; ?>" />
                        <?php echo JHtml::_( 'form.token' ); ?>
                    </div>
                </div>


                </div>
            </div>
        </fieldset>
    </form>
</div>


<script>
jQuery(document).ready(function(){
    jQuery('#jform_contact_name ,#jform_contact_email,#jform_contact_subject,#jform_contact_message').addClass('form-control');
});
</script>
